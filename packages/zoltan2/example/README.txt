Examples
========

Zoltan2 is written in C++ and uses templated classes.  Many Zoltan users are unfamiliar with C++, and so the Zoltan2 developers have tried to include examples that show the best ways to use the library if you are not a C++ programmer.

Examples with names appended "_C" are written with C-language programmers in mind.  Those appended with "_CPP" are written for C++-language programmers.

In addition, Zoltan2 has features that make it particularly easy to work with if you are using Trilinos classes to represent matrices, graphs or geometry.  Examples that show how to use these features have names ending with "_Trilinos".

Examples will be compiled when the cMake configure option "-D Zoltan2_ENABLE_EXAMPLES:BOOL=on" is specified.
