#!/bin/bash
#-----------------------------------------------------------------------
#      phdMesh : Parallel Heterogneous Dynamic unstructured Mesh
#                Copyright (2004) Sandia Corporation
#
# Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
# license for use of this work by or on behalf of the U.S. Government.
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2.1 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
# USA
#-----------------------------------------------------------------------

THIS_FILE=${PHDMESH_PATH}/config/gnu

export PHDMESH_CONFIG_DEPS="${PHDMESH_CONFIG_DEPS} ${THIS_FILE}"

#-----------------------------------------------------------------------

export AR="ar"

export ARFLAGS="rc"

export RANLIB="ranlib"

#-----------------------------------------------------------------------

export CC="gcc"
export CXX="g++"
export LDCXX=${CXX}

INCLUDE_PATH="-I. -I${PHDMESH_PATH}/include"

CFLAGS_DIAG="-Wall -Wextra -Werror"
CXXFLAGS_DIAG="-Wall -Wextra -Werror -Woverloaded-virtual"

export CFLAGS="  -std=c99   ${CFLAGS}   ${CFLAGS_DIAG}   ${INCLUDE_PATH}"
export CXXFLAGS="-std=c++98 ${CXXFLAGS} ${CXXFLAGS_DIAG} ${INCLUDE_PATH}"

export C_LIB_EXT="${C_LIB_EXT}     -lpthread"
export CXX_LIB_EXT="${CXX_LIB_EXT} -lpthread"

#-----------------------------------------------------------------------

while [ -n "${1}" ] ; do

case ${1} in
DEBUG | debug | DBG | dbg )
  shift 1
  export CFLAGS="${CFLAGS} -g"
  export CXXFLAGS="${CXXFLAGS} -g"
  ;;
OPT* | opt* )
  shift 1
  export CFLAGS="${CFLAGS} -O3"
  export CXXFLAGS="${CXXFLAGS} -O3"
  ;;
32BIT | 32bit )
  shift 1
  export CFLAGS="${CFLAGS} -Wa,--32 -m32"
  export CXXFLAGS="${CXXFLAGS} -Wa,--32 -m32"
  ;;
STATIC | static )
  shift 1
  export CFLAGS="${CFLAGS} -static"
  export CXXFLAGS="${CXXFLAGS} -static"
  ;;
MPICH | mpich )
  shift 1
  export CC="mpicc"
  export CXX="mpiCC"
  export LDCXX=${CXX}
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define HAVE_MPI\n"
  ;;
MPICHXX | mpichxx )
  shift 1
  export CC="mpicc"
  export CXX="mpicxx"
  export LDCXX=${CXX}
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define HAVE_MPI\n"
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define MPICH_SKIP_MPICXX\n"
  ;;
PURIFY | purify )
  shift 1
  export LDCXX="purify ${CXX}"
  ;;
QUANTIFY | quantify )
  shift 1
  export LDCXX="quantify ${CXX}"
  ;;
MPICH_PURIFY | mpich_purify )
  shift 1
  MPICH_C_LIB="-L${1} -lmpich -lpthread -lrt"
  MPICH_CXX_LIB="-L${1} -lpmpich++ -lmpich -lpthread -lrt"
  shift 1
  export CC="mpicc"
  export CXX="mpiCC"
  export LDCXX="purify g++"
  export C_LIB_EXT="${C_LIB_EXT} ${MPICH_C_LIB}"
  export CXX_LIB_EXT="${CXX_LIB_EXT} ${MPICH_CXX_LIB}"
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define HAVE_MPI\n"
  ;;
MPIGM | mpigm )
  MPIGM_LIB="-L${2} -lgm"
  shift 2
  export CC="mpicc"
  export CXX="mpiCC"
  export LDCXX=${CXX}
  export C_LIB_EXT="${C_LIB_EXT} ${MPIGM_LIB}"
  export CXX_LIB_EXT="${CXX_LIB_EXT} ${MPIGM_LIB}"
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define HAVE_MPI\n"
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define LINUX64_WS32_GM\n"
  ;;
NO_SCHED | no_sched )
  shift 1
  export PHDMESH_DEFINES="${PHDMESH_DEFINES}#define TPI_NO_SCHED\n"
  ;;
*)
  echo ${THIS_FILE} 'unknown option = ' ${1}
  exit -1
  ;;
esac

done

${CXX} --version
${CC} --version

#-----------------------------------------------------------------------

source ${PHDMESH_PATH}/config/fortran LOWER_

#-----------------------------------------------------------------------

