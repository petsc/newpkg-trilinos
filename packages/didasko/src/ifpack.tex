% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{Incomplete Factorizations with IFPACK}
\label{chap:ifpack}

\ChapterAuthors{Marzio Sala, Michael Heroux, David Day}

\begin{introchapter}
IFPACK provides a suite of object-oriented algebraic preconditioners for
the solution of preconditioned iterative solvers. IFPACK offers a
variety of overlapping (one-level) Schwarz preconditioners. The package
uses Epetra for basic matrix-vector calculations, and accepts user
matrices via an abstract matrix interface. A concrete implementation for
Epetra matrices is provided. The package separates graph construction
from factorization, improving performance substantially compared to other factorization packages.

In this Chapter we discuss the use of IFPACK objects as preconditioners for
AztecOO solvers.  Specifically we present:
\begin{itemize}
\item Parallel distributed memory issues (in Section~\ref{sec:ifpack_parallel}).
\item Incomplete factorizations and notation (in Section~\ref{sec:ifpack_theoretical}).
\item How to compute incomplete Cholesky factorizations (in
  Section~\ref{sec:ifpack_chol}).
\item IFPACK's RILU-type factorizations (in
  Section~\ref{sec:ifpack_rilu}).
\end{itemize}
\end{introchapter}

%%%
%%%
%%%

\section{Theoretical Background}
\label{sec:ifpack_theoretical}

The aim of this section is to define concepts associated with incomplete
factorization methods and establish our notation. This section is not
supposed to be exhaustive, nor complete on this subject. The reader is
referred to the existing literature for a comprehensive presentation.

\medskip

A broad class of effective preconditioners is based on incomplete
factorization of the linear system matrix.  Such preconditioners are often
referred to as incomplete lower/upper (ILU) preconditioners.  
ILU preconditioning techniques lie between direct and
iterative methods and provide a balance between reliability and
numerical efficiency.  ILU preconditioners are constructed in the factored form
$P=\tilde{L} \tilde{U}$, with $\tilde{L}$ and $\tilde{U}$ being lower
and upper triangular matrices. Solving with $P$ involves two triangular
solutions.

ILU preconditioners are based on the observation
that, although most matrices $A$ admit an LU factorization $A=LU$, where $L$ is
(unit) lower triangular and $U$ is upper triangular, the factors $L$ and $U$ often
contain too many nonzero terms, making the cost of factorization too expensive in
time or memory use, or both.  One type of ILU preconditioner is ILU(0), which 
is defined as proceeding through the standard LU decomposition computations, but keeping 
only those terms in $\tilde{L}$ that correspond to nonzero terms in the lower
triangle of $A$ and similarly keeping only those terms in $\tilde{U}$ that 
correspond to nonzero terms in the upper triangle of $A$.  Although effective, in
some cases the accuracy of the ILU(0) may be insufficient to yield an
adequate rate of convergence. More accurate factorizations will differ
from ILU(0) by allowing some {\em fill-in}. The resulting class of
methods is called ILU($k$), where $k$ is the level-of-fill. A
level-of-fill is attributed to each element that is processed by
Gaussian elimination, and dropping will be based on the level-of-fill.
The level-of-fill should be indicative of the size of the element: the
higher the level-of-fill, the smaller the elements.  

Other strategies consider dropping by value -- for example, dropping
entries smaller than a prescribed threshold. Alternative dropping
techniques can be based on the numerical size of the element to be
discarded. Numerical dropping strategies generally yield more accurate
factorizations with the same amount of fill-in as level-of-fill
methods. The general strategy is to compute an entire row of the
$\tilde{L}$ and $\tilde{U}$ matrices, and then keep only a certain
number of the largest
entries. In this way, the amount of fill-in is
controlled; however, the structure of the resulting matrices is
undefined. These factorizations are usually referred to as ILUT, and a
variant which performs pivoting is called ILUTP.  

When solving a single linear system, ILUT methods can be more effective
than ILU($k$).  However, in many situations a sequence of linear systems
must be solved where the pattern of the matrix $A$ in each system is
identical but the values of changed.  In these situations, ILU($k$) is 
typically much more effective because the pattern of ILU($k$) will also
be the same for each linear system and the overhead of computing the
pattern is amortized.

\section{Parallel Incomplete Factorizations}
\label{sec:ifpack_parallel}

Parallel direct sparse solvers that compute the complete factorization $A=LU$
are effective on parallel computers.  However, the effective scalability
of these solvers is typically limited to a speedup of order ten, regardless
of the number of processors used.  Also, it is typically the factorization
(constructing $L$ and $U$) that exhibits the best parallel speedup.  The 
forward and back triangular solves typically exhibit very poor parallel speedup.

The situation for ILU preconditioners is even worse.  Complete factorizations
can scale well because of very important graph properties that can be determined
at low cost.  ILU factorizations do not have the same properties, so predicting
fill-in across the parallel machine is not practically possible.  Also, because ILU
preconditioners require repeated forward and back solves, they are more affected
by the poor scalability of these operations.

Because ILU preconditioners do not scale well on parallel computers, a common
practice is to perform {\em local} ILU factorizations.  In this situation, each
processor computes a factorization of a subset of matrix rows and columns independently
from all other processors.  This additional layer of approximation leads to a block
Jacobi type of preconditioner across processors, where each block is solved using an
ILU preconditioner.  The difficulty with this type of preconditioner is that it 
tends to become less robust and require more iterations as the number of processors used
increases.  This effect can be offset to some extent by allowing {\em overlap}.  Overlap
refers to having processors redundantly own certain rows of the matrix for the ILU
factorization.  Level-1 overlap is defined so that a processor will include rows that
are part of its original set.  In addition, if row $i$ is part of its original set and 
row $i$ of $A$ has a nonzero entry in column $j$, then row $j$ will also
be included in the factorization on that processor.  Other levels of overlap are
computed recursively.  IFPACK supports an arbitrary level of overlap.  However,
level-1 is often most effective.  Seldom more than 3 levels are needed. 

%%%
%%%
%%%

\section{Incomplete Cholesky Factorizations}
\label{sec:ifpack_chol}

Recall that if a matrix is symmetric positive definite, it admits a Cholesky
factorization of the form $A=LL^T$, where $L$ is lower triangular.
Ifpack\_CrsIct is a class for constructing and using incomplete Cholesky
factorizations of an Epetra\_CrsMatrix. It is built in part on top of the ICT
preconditioner developed by Edmond Chow at Lawrence Livermore National
Laboratory~\cite{ChowICT}.  Specific factorizations depend on several parameters:
\begin{itemize}
\item Maximum number of entries per row/column. The factorization
  will contain at most this number of nonzero elements in each
  row/column;
\item Diagonal perturbation.  By default, the factorization will be
  computed on the input matrix. However, it is possible to modify the
  diagonal entries of the matrix to be factorized, via functions
  \verb!SetAbsoluteThreshold()! and \verb!SetRelativeThreshold()!. Refer
  to the IFPACK's documentation for more details.
\end{itemize}

It is easy to have IFPACK compute the incomplete factorization. First, define
an Ifpack\_CrsIct object,
\begin{verbatim}
Ifpack_CrsIct * ICT = NULL;
ICT = Ifpack_CrsIct(A,DropTol,LevelFill);
\end{verbatim}
where \verb!A! is an Epetra\_CrsMatrix (already FillComplete'd), and
\verb!DropTop! and \verb!LevelFill! are the drop tolerance and the
level-of-fill, respectively. Then, we can set the values and compute the
factors,
\begin{verbatim}
ICT->InitValues(A);
ICT->Factor();
\end{verbatim}

IFPACK can compute the estimation of the condition number
\[
cond(L_i U_i) \approx \|(LU)^{-1} e \|_\infty ,
\]
where $e = (1,1,\dots,1)^T$. (More details can be found in the IFPACK
documentation.) This estimation can be computed as follows:
\begin{verbatim}
double Condest;
ICT->Condest(false,Condest);
\end{verbatim}
Please refer to file \TriExe{ifpack/ex1.cpp} for a complete example of
incomplete Cholesky factorization.

%%%
%%%
%%%

\section{RILUK  Factorizations}
\label{sec:ifpack_rilu}

IFPACK implements various incomplete factorization for non-symmetric
matrices. In this Section, we will consider the Epetra\_CrsRiluk class,
that can be used to produce RILU factorization of a Epetra\_CrsMatrix.
The class required an Ifpack\_OverlapGraph in the construction phase.
This means that the factorization is split into two parts:
\begin{enumerate}
\item Definition of the level filled graph;
\item Computation of the factors.
\end{enumerate}
This approach can significantly improve the performances of code, when
an ILU preconditioner has to be computed for several matrices, with
different entries but with the same sparsity pattern. An
Ifpack\_IlukGraph object of an Epetra matrix \verb!A! can be constructed
as follows:
\begin{verbatim}
Ifpack_IlukGraph Graph = 
  Ifpack_IlukGraph(A.Graph(),LevelFill,LevelOverlap);
\end{verbatim}
Here, \verb!LevelOverlap! is the required overlap among the subdomains.

A call to \verb!ConstructFilledGraph()! completes the process.

\begin{remark}
  An Ifpack\_IlukGraph object has two Epetra\_CrsGraph objects,
  containing the $L_i$ and $U_i$ graphs. Thus, it is possible to
  manually insert and delete graph entries in $L_i$ and $U_i$ via the
  Epetra\_CrsGraphInsertIndices and RemoveIndices functions. However, in
  this case FillComplete must be called before the graph is used for
  subsequent operations.
\end{remark}

At this point, we can create an Ifpack\_CrsRiluk object,
\begin{verbatim}
ILUK = Ifpack_CrsRiluk(Graph);
\end{verbatim}
This phase defined the graph for the incomplete factorization, without
computing the actual values of the $L_i$ and $U_i$ factors. Instead,
this operation is accomplished with
\begin{verbatim}
int initerr = ILUK->InitValues(A);
\end{verbatim}
The ILUK object can be used with AztecOO simply setting
\begin{verbatim}
solver.SetPrecOperator(ILUK);
\end{verbatim}
where \verb!solver! is an AztecOO object.
\smallskip
The example in \\ \TriExe{ifpack/ex2.cpp} shows the use of the Ifpack\_CrsRiluk class.

\medskip

The application of the incomplete factors to a global vector, $z =
(L_iU_i^{-1}) r$, results in redundant approximation for any element of
$z$ that correspond to rows that are part of more than one local ILU
factor. The OverlapMode defines how those redundant values are managed.
OverlapMode is an Epetra\_CombinedMode enum, that can assume the
following values: {\tt Add, Zero, Insert, Average, AbxMax}. The default
is to zero out all the values of $z$ for rows that were not part of the
original matrix row distribution.

%A technique, sometimes called relaxation, can be used to obtain RILU
%factorization. If RelaxValue (set by \verb!SetRelaxValue()!) is
%different from zero, the extra-term, dropped during the factorization,
%are not dropped. This can be of help for operators that should have zero
%row-sum. 

%%%
%%%
%%%


\section{Concluding Remarks on IFPACK}
\label{sec:ifpack_concluding}

More documentation on the IFPACK package can be found in
\cite{ifpack-guide}
