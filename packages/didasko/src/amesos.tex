% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{Interfacing Direct Solvers with Amesos}
\label{chap:amesos}

\ChapterAuthors{Marzio Sala}

\begin{introchapter}
The Amesos package provides an object-oriented interface to several
direct sparse solvers. Amesos will solve (using a direct factorization
method) the linear systems of equations (\ref{eq:linear_sys}) where $A$
is stored as an Epetra\_RowMatrix object, and $X$ and $B$ are
Epetra\_MultiVector objects.

The Amesos package has been designed to face some of the challenges of
direct solution of linear systems. In fact, many solvers have been
proposed in the last years, and often each of them requires different
input formats for the linear system matrix. Moreover, it is not uncommon
that the interface changes between revisions. Amesos aims to solve those
problems, furnishing a clean, consistent interface to many direct
solvers.
\end{introchapter}

\section{Introduction to the Amesos Design}

Using Amesos, users can interface their codes with a (large) variety of
direct linear solvers, sequential or parallel, simply by a code
instruction of type
\begin{verbatim}
AmesosProblem.Solver();
\end{verbatim}
Amesos will take care of redistributing data among the processors, if
necessary.


All the Amesos classes are derived from a base class mode,
\verb!Amesos_BaseSolver!. This abstract interface provides the basic
functionalities for all Amesos solvers, and allows users to choose
different direct solvers very easily -- by changing an input scalar
parameter. See Section~\ref{sec:amesos_generic} for more details.

In this Chapter, we will suppose that matrix $A$ in
equation~(\ref{eq:linear_sys}) is defined as an Epetra\_RowMatrix, in
principle with nonzero entries on all the processes defined in the
Epetra\_Comm communicator in use. $X$ and $B$, instead, are
Epetra\_MultiVector, defined on the same communicator.  

Amesos contains several classes: 
\begin{itemize}
\item \verb!Amesos_Lapack!: Interface to serial dense solver LAPACK.
\item \verb!Amesos_KLU!: Interface to Amesos's internal solver KLU. KLU
  is a serial, unblocked code ideal for getting started, and for very
  sparse matrices, such as circuit matrices. 
\item \verb!Amesos_Umfpack!: Interface to Tim Davis's
  UMFPACK~\cite{umfpack}. UMFPACK is a serial solver.
%\item \verb!Amesos_Dscpack!: Interface to Padma Raghavan's
%  DSCPACK~\cite{dscpack-home-page};
\item \verb!Amesos_Superludist!: Interface to Xiaoye S.~Li's SuperLU
  solver suite, including SuperLU, SuperLU\_DIST 1.0 and SuperLU\_DIST
  2.0~\cite{superlu-home-page}. SuperLU is a serial solvers, while
  SuperLU\_DIST is a parallel solver.
\item \verb!Amesos_Mumps!: Interface to MUMPS
  4.3.1~\cite{mumps-home-page}\footnote{At present, MUMPS is the only
    Amesos class that can take advantage of the symmetry of the linear
    system matrix.}. MUMPS is a parallel direct solver;
\item \verb!Amesos_Scalapack!: Interface to ScaLAPACK~\cite{scalapack},
  the parallel version of LAPACK\footnote{Note that Amesos does {\sl
      not} contain interfaces to LAPACK routines.  Other Trilinos
    packages already offer those routines (Epetra and Teuchos).  }.
\end{itemize}

If the supported packages is serial, and one is solving with more than
one process, matrix and right-hand side are shipped to process 0,
solved, then the solution is broadcasted to the distributed solution
vector $X$. For parallel solvers, instead, various options are
supported, depending on the package at hand:
\begin{itemize}
\item The Amesos\_Superludist interface can be used over all the
  processes, as well as on a subset of them. The matrix is kept in
  distributed form over the processes of interest;
\item Amesos\_Mumps can keep the matrix in a distributed form over all
  the processes, or the matrix can be shipped to processor 0. In both
  cases, all the processes in the MPI communicator will be used.
\end{itemize}

This Chapter, we will cover:
\begin{itemize}
\item The Amesos\_BaseSolver interface to various direct solvers,
  presented (in Section~\ref{sec:amesos_generic}).
\end{itemize}

%%%
%%%
%%%

\section{Amesos\_BaseSolver: A Generic Interface to Direct Solvers}
\label{sec:amesos_generic}

All Amesos objects are constructed from the function class
\verb!Amesos!.  Amesos allows a code to delay the
decision about which concrete class to use to implement the
Amesos\_BaseSolver interface. The main goal of this class is to allow
the user to select any supported (and enabled at configuration time)
direct solver, simply changing an input parameter. Another remarkable
advantage of Amesos\_BaseSolver is that, using this class, users does
not have to include the header files of the 3-part libraries in their
code\footnote{Using Amesos\_BaseSolver, 3-part libraries header files
  are required in the compilation of Amesos only.}.

An example of use of this class is as follows. First, the following
header files must be included:
\begin{verbatim}
  #include "Amesos.h" 
  #include "AmesosClassType.h"
\end{verbatim}
Then, let \verb!A! be an Epetra\_RowMatrix object (for instance, and
Epetra\_CrsMatrix). We need to define a linear problem,
\begin{verbatim}
  Epetra_LinearProblem * Amesos_LinearProblem = 
                         new Epetra_LinearProblem;
  Amesos_LinearProblem->SetOperator( A ) ; 
\end{verbatim}
and to create an Amesos parameter list (which can be empty):
\begin{verbatim}
  Teuchos::ParameterList ParamList ;
\end{verbatim}
Now, let \verb!Choice! be an string, with one of the
following values: 
\begin{itemize}
\item {\tt AMesos\_Klu};
\item {\tt Amesos\_Umfpack};
\item {\tt Amesos\_Mumps};
\item {\tt Amesos\_Superludist};
\item {\tt Amesos\_Scalapack}.
\end{itemize}
We can construct an \verb!Amesos_BaseSolver! object as follows:
\begin{verbatim}
  Amesos_BaseSolver * A_Base;
  Amesos A_Factory;

  A_Base = A_Factory.Create(Choice, *Amesos_LinearProblem, 
                            ParamList );
  assert(A_Base!=0);
\end{verbatim}
Symbolic and numeric factorizations are computed using methods
\begin{verbatim}
  A_Base->SymbolicFactorization();
  A_Base->NumericFactorization();
\end{verbatim}
The numeric factorization phase will check whether a symbolic
factorization exists or not. If not, method
\verb!SymbolicFactorization()! is invoked.  Solution is computed (after
setting of LHS and RHS in the linear problem), using
\begin{verbatim}
  A_Base->Solve();
\end{verbatim}
The solution phase will check whether a numeric factorization exists or
not. If not, method \verb!SymbolicFactorization()! is called.

Users must provide the nonzero structure of the matrix for the symbolic
phase, and the actual nonzero values for the numeric
factorization. Right-hand side and solution vectors must be set before
the solution phase, for instance using
\begin{verbatim}
  Amesos_LinearProblem->SetLHS(x);
  Amesos_LinearProblem->SetRHS(b);
\end{verbatim}

A common ingredient to all the Amesos classes is the Teuchos parameters'
list. This object, whose definition requires the input file
\verb!Teuchos_ParameterList.hpp!, is used to specify the parameters that
affect the 3-part libraries. Here, we simply recall that the parameters'
list can be created as
\begin{verbatim}
  Teuchos::ParameterList AmesosList;
\end{verbatim}
and parameters can be set as
\begin{verbatim}
  AmesosList.set(ParameterName,ParameterValue);
\end{verbatim}
Here, \verb!ParameterName! is a string containing the parameter name,
and \verb!ParameterValue! is any valid C++ object that specifies the
parameter value (for instance, an integer, a pointer to an array or to
an object).

For a detailed list of parameters we refer to~\cite{Amesos-Reference-Guide}.
