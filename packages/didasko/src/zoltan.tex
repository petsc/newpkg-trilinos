% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{Partitioning and Load Balancing with Isorropia and Zoltan}
\label{chap:zoltan}

\ChapterAuthors{Erik G. Boman}

\begin{introchapter}
%\emph{This section is out of date! The Isorropia package is now the preferred way to access Zoltan from other Trilinos packages. Zoltan itself will be available as a Trilinos package starting with the Trilinos 9.0 release.} 
 
In order to get good parallel performance, the data 
distribution (map) is important. Poor data distribution can both
lead to high communication among processes and also load imbalance,
that is, some processes have more work than others. 

Trilinos provides two packages for partitioning and load balancing: Zoltan and Isorropia. While Zoltan has a generic, data-structure-neutral interface, Isorropia provides Epetra interfaces to Zoltan that are more convenient for many Trilinos users.
\end{introchapter}

\section{Background}
In parallel linear algebra applications, a critical part is to 
distribute the matrices among processes (processors). The vectors are 
often distributed to conform
with the appropriate matrices, though not always. Matrices can
be partitioned either along rows, columns, or by a 2-dimensional
block decomposition. We limit our discussion to 1-dimensional data
distributions, in particular, row distributions  (which are best supported 
in Epetra). In this case, partitioning dense matrices is easy.
For a matrix with $n$ rows and with $p$ processes, simply give
each process $n/p$  rows. For sparse matrices, the situation
is more complicated. To achieve load-balance, one may wish 
that each process obtains approximately the same number of rows,
or alternatively, similar number of nonzero entries. 
Additionally, the communication cost when applying the matrix
should be small. Specifically, for iterative solvers, the
communication cost in a matrix-vector product should be minimized.

A common abstraction of this problem is \emph{graph partitioning}.
This model assumes the matrix is symmetric, so the sparsity 
pattern of the matrix can be represented by an undirected graph.
The graph partitioning problem is to partition the
vertices into $p$ sets such that the number of edges between
sets are minimized. The number of cut edges approximates the
communication cost in the parallel computation. Although 
graph partitioning is NP-hard to solve exactly, there are
several fast algorithms that work well in practice. Zoltan
provides a common interface to graph partitioners (and other algorithms).
At present, the most widely used software for graph partitioning,
are the METIS and ParMETIS \cite{Metis,KarypisK99} packages from University 
of Minnesota.

Recently, it has been shown \cite{CatAyk99} that hypergraph partitioning 
is a more accurate model for parallel matrix-vector communication cost.
A parallel hypergraph partitioner is available in Zoltan~\cite{ZoltanParHyp06ipdps,ZoltanIPDPS07}
An advantage of hypergraph partitioning is that it
also applies to rectangular matrices, not just square matrices.

\section{Partitioning Methods}
\label{sec:methods}
Zoltan and Isorropia currently support two types of partitioning methods: geometric and graph/hypergraph. The geometric methods are convenient if you have a vector of points in space, for example particles, or mesh points. The partitioner will then partition these points such that points that are close in Euclidean space will be assigned to the same or a nearby process. 

The graph/hypergraph methods do not use geometry but rather rely on connectivity information, e.g., the sparsity pattern of a matrix. Multilevel algorithms for (hyper-)graph partitioning give high-quality partitionings, but take longer to compute than geometric methods.

\section{Isorropia}
\label{sec:isorropia}
Isorropia is the preferred partitioning package for most Trilinos users
since it supports Epetra. The actual partitioning is done by calling Zoltan,
so Zoltan is a required dependency.

Isorropia provides a simple interface for new users: \texttt{createBalancedCopy}. The input is an Epetra distributed data object (matrix, vector), and the output is a copy of the object (matrix, vector) but with a different (better) map (distribution):
\begin{verbatim}
  Epetra_CrsMatrix *A;    // Original matrix
  Epetra_CrsMatrix *bal_A; // Balanced matrix
  bal_A = Isorropia::createBalancedCopy(A);
\end{verbatim}
Note that the user is responsible for deallacating \texttt{balA} after use.
The \texttt{createBalancedCopy} interface is rather limited and mainly intended for beginners. The full-fledged (primary) API contains the following three classes:
\begin{enumerate}
\item Partitioner
\item Redistributor
\item CostDescriber
\end{enumerate}
The Partitioner performs the partitioning, but does not move any data. The Redistributor takes user data and a Partitioner as input, and redistributes the user data. The CostDescriber is optional and can be used to provide costs (weights) to the Partitioner for problem-specific partitioning. 

Note that the primary API uses Teuchos::RCP reference-counted pointers for safer memory management.

\begin{verbatim}
  Epetra_CrsMatrix A;
  Partitioner part(Teuchos::rcp(A));
  Redistributor rd(Teuchos::rcp(part));
  Teuchos::RCP<Epetra_CrsMatrix> bal_A = rd.redistribute(A);
\end{verbatim}

The default partitioning method in Isorropia is hypergraph partitioning for a sparse matrix, and RCB for a vector.

Isorropia only supports a small number of parameters. The parameters are case insensitive.  Advanced users who wish to specify detailed options should use Zoltan parameters. Isorropia supports a parameter sublist \emph{Zoltan}, and these parameters are passed on to Zoltan.


\section{Zoltan}
\label{sec:zoltan}
Zoltan is a general-purpose package for parallel data management, including partitioning and load balancing~\cite{zoltan2002}. Zoltan was developed independently of Trilinos, and does not depend on any other Trilinos packages. Thus, it can be built and used as a stand-alone library, if desired. Zoltan was written in C but also provides C++ and Fortran90 wrapper interfaces.

Zoltan currently provides all the partitioning methods in Isorropia. Zoltan also supports several third-party libraries, including the popular graph partitioners ParMetis and Scotch. To build Zoltan with ParMetis, you need to turn on \texttt{TPL\_ENABLE\_parmetis} in Cmake. Zoltan TPLs can also be used via Isorropia.

