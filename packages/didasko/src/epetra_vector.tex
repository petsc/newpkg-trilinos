% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{Working with Epetra Vectors}
\label{chap:epetra_vec}

\ChapterAuthors{Marzio Sala, Michael Heroux, and David Day.}

\begin{introchapter}
A vector is a fundamental data structure required by almost all
numerical methods. Within the Trilinos framework, vectors are usually
constructed starting from Epetra classes.

An Epetra vector may store either double-precision values (like the
solution of a PDE problem, the right-hand side of a linear system, or
nodal coordinates), or integer data values (such as a set of indexes or
global IDs).

An Epetra vector may be either {\em serial} or {\em distributed}. Serial
vectors are usually small, so that it is not convenient to distribute
them across the processes. Possibly, serial vectors are replicated
across the processes. On the other hand, distributed vectors tend to be
significantly larger, and therefore their elements are distributed
across the processors. In this latter case, users must specify the
partition they intend to use.  In Epetra, this is done by specifying a
communicator (introduced in Section~\ref{sec:comm}) and an Epetra object
called map (introduced in Section~\ref{sec:map}). A map is basically a
partitioning of a list of global IDs.

\medskip

During the Chapter, the user will be introduced to:
\begin{itemize}
\item The fundamental Epetra communicator object, Epetra\_Comm (in
  Section~\ref{sec:comm});
\item The Epetra\_Map object (in Section~\ref{sec:map});
\item The creation and assembly of Epetra vectors (in
  Sections~\ref{sec:serial_vec} and \ref{sec:distr_vec}). The sections
  also present common vector operations, such as dot products, fill with
  constant or random values, vector scalings and norms;
\item A tool to redistributing vectors across processes (in
  Section~\ref{sec:import_export}).
\end{itemize}
\end{introchapter}

%%%
%%%
%%%

\section{Epetra Communicator Objects}
\label{sec:comm}

The Epetra\_Comm virtual class is an interface that encapsulates the
general information and services needed for the other Epetra classes to
run on serial or parallel computer. An Epetra\_Comm object is required
for building all Epetra\_Map objects, which in turn are required for all
other Epetra classes.

Epetra\_Comm has two basic concrete implementations:
\begin{itemize}
\item Epetra\_SerialComm (for serial executions);
\item Epetra\_MpiComm (for MPI distributed memory executions).
\end{itemize}

For most basic applications, the user can create an Epetra\_Comm object
using the following code fragment:
\begin{verbatim}
#include "Epetra_ConfigDefs.h"
#ifdef HAVE_MPI
#include "mpi.h"
#include "Epetra_MpiComm.h"
#else
#include "Epetra_SerialComm.h"
#endif
// .. other include files and others ...
int main( int argv, char *argv[]) {
 // .. some declarations here ...
#ifdef HAVE_MPI
  MPI_Init(&argc, &argv);
  Epetra_MpiComm Comm(MPI_COMM_WORLD);
#else
  Epetra_SerialComm Comm;
#endif
// ... other code follows ...
\end{verbatim}
Note that the \verb!MPI_Init()! call and the
\begin{verbatim}
#ifdef HAVE_MPI
  MPI_Finalize();
#endif
\end{verbatim}
call, are likely to be the {\em only} MPI calls users have to explicitly
introduce in their code.

Most of Epetra\_Comm methods are similar to MPI functions. The class
provides methods such as \verb!MyPID()!, \verb!NumProc()!,
\verb!Barrier()!, \verb!Broadcast()!, \verb!SumAll()!,
\verb!GatherAll()!, \verb!MaxAll()!, \verb!MinAll()!, \verb!ScanSum()!.
For instance, the number of processes in the communicator,
\verb!NumProc!, and the ID of the calling process, \verb!MyPID!, can be
obtained by
\begin{verbatim}
int NumProc = Comm.NumProc();
int MyPID = Comm.MyPID();
\end{verbatim}

The file \TriExe{epetra/ex1.cpp} presents the use of some of the above
introduced functions.  For a description of the syntax, please refer to
the Epetra Class Documentation.

\begin{table}
\begin{center}
\begin{tabular}{ | p{15cm} | }
\hline
\verb!NumGlobaElementss()!\\
The total number of elements across all processes. \\
\\
\verb!NumMyElementss()!\\
The number of elements on the calling process. \\
\\
 \verb!MinAllGID()! \\
The minimum global index value across all processes.  \\
\\
 \verb!MaxAllGID()! \\
The maximum global index value across all processes. \\
\\
 \verb!MinMyGID()!\\
The minimum global index value on the calling process. \\
\\
 \verb!MaxMyGID()! \\
The maximum global index value on the calling process. \\
\\
 \verb!MinLID()! \\
The minimum local index value on the calling process. \\
\\
 \verb!MaxLID()! \\
The maximum local index value on the calling process. \\
\\
 \verb!LinearMap()! \\
Returns true if the elements are distributed linearly across processes,
i.e., process 0 gets the first n/p elements, process 1 gets the next
n/p elements, etc. where n is the number of elements and p is the number
of processes.  \\
\\
\bf \verb!DistributedGlobal()! \\
Returns true if the element space of the map spans more than one
process. This will be true in most cases, but will be false in serial
cases and for objects that are created via the derived Epetra\_LocalMap
class.  \\
\hline
\end{tabular}
\caption{Some methods of the class {\tt Epetra\_Map}}
\label{tab:epetra_map}
\end{center}
\end{table}

%%%
%%%
%%%

\section{Defining a Map}
\label{sec:map}

The distribution of a set of integer labels (or elements) across the
processes is here called a {\sl map}, and its actual implementation is
given by the Epetra\_Map class (or, more precisely, by an
Epetra\_BlockMap, from which Epetra\_Map is derived).  Basically, the
class handles the definition of the:
\begin{itemize}
\item global number of elements in the set (called
  \verb!NumGlobalElements!);
\item local number of elements (called \verb!NumMyElements!);
\item global numbering of all local elements (an integer vector of size
  \verb!NumMyElements!, called \verb!MyGlobalElements!).
\end{itemize}

There are  three ways to define an map. The easiest way is to
specify the global number of elements, and let Epetra decide:
\begin{verbatim}
Epetra_Map Map(NumGlobalElements,0,Comm);
\end{verbatim}
In this case, the constructor takes the global dimension of the vector,
the base index\footnote{The index base is the index of the lowest order
  element, and is usually, {\tt 0} for C or C++ arrays, and {\tt 1} for
  FORTRAN arrays. Epetra can indeed accept any number as index base.
  However, some other Trilinos package may require a C-style index
  base.}, and an \verb!Epetra_Comm!  object (introduced in
Section~\ref{sec:comm}). As a result, each process will be assigned a
contiguous set of elements.

A second way to build the Epetra\_Comm object is to furnish the local
number of elements:
\begin{verbatim}
Epetra_Map Map(-1,NumMyElements,0,Comm);
\end{verbatim}
This will create a vector of size $\sum_{i=0}^{NumProc-1}$
\verb!NumMyElements!. Each process will get a contiguous set of elements.
These two approached are coded in file \newline \TriExe{epetra/ex2.cpp}.

A third more involved way to create an Epetra\_Map, is to specify on
each process both the number of local elements, and the global indexing
of each local element. To understand this, consider the following code.
A vector of global dimension 5 is split among processes \verb!p0! and
\verb!p1!. Process \verb!p0! owns elements 0 an 4, and process \verb!p1!
elements 1, 2, and 3.
\begin{verbatim}
#include "Epetra_Map.h"
// ...
MyPID = Comm.MyPID();
switch( MyPID ) {
case 0:
  MyElements = 2;
  MyGlobalElements = new int[MyElements];
  MyGlobalElements[0] = 0;
  MyGlobalElements[1] = 4;
  break;
case 1:
  MyElements = 3;
  MyGlobalElements = new int[MyElements];
  MyGlobalElements[0] = 1;
  MyGlobalElements[1] = 2;
  MyGlobalElements[2] = 3;
  break;
}

Epetra_Map Map(-1,MyElements,MyGlobalElements,0,Comm);
\end{verbatim}
The complete code is reported in \TriExe{epetra/ex3.cpp}.

Once created, a Map object can be queried for the global and local
number of elements, using
\begin{verbatim}
int NumGlobalElements = Map.NumGlobalElements();
int NumMyElements = Map.NumMyElements();
\end{verbatim}
and for the global ID of local elements, using
\begin{verbatim}
int * MyGlobalElements = Map.MyGlobalElements();
\end{verbatim}
that returns a pointer to the internally stored global indexing vector,
or, equivalently,
\begin{verbatim}
int MyGlobalElements[NumMyElements];
Map.MyGlobalElements(MyGlobalElements);
\end{verbatim}
that copies in the user's provided array the global indexing. 

 \bigskip

The class Epetra\_Map is derived from Epetra\_BlockMap. The class keeps
information that describes the distribution of objects that have block
elements (for example, one or more contiguous entries of a vector). This
situation is common in applications like multiple-unknown PDE problems.
A variety of constructors are available for the class. An example of the
use of block maps is reported in \TriExe{epetra/ex23.cpp}.

\smallskip

Note that different maps may coexist in the same part of the code.  The
user may define vectors with different distributions (even for vectors
of the same size).  Two classes are provided to transfer data from one
map to an other: Epetra\_Import and Epetra\_Export (see
Section~\ref{sec:import_export}).

\begin{remark}
Most Epetra objects overload the \verb!<<! operator. For example, to
visualize information about the \verb!Map!, one can simply write
\begin{verbatim}
cout << Map;
\end{verbatim}
\end{remark}

We have constructed very basic map objects.  More general objects can be
constructed as well. First, element numbers are only labels, and they do
not have to be consecutive.  This means that we can define a map with
elements 1, 100 and 10000 on process 0, and elements 2, 200 and 20000 on
process 1. This map, composed by 6 elements, is perfectly legal. Second,
each element can be assigned to more than one process. 
Examples \newline
\TriExe{epetra/ex20.cpp} and \newline \TriExe{epetra/ex21.cpp} can be used to
better understand the potential of Epetra\_Maps.

\begin{remark}
  The use of ``distributed directory'' technology facilitates arbitrary
  global ID support.
\end{remark}

%%%
%%%
%%%

\section{Creating and Assembling Serial Vectors}
\label{sec:serial_vec}

Within Epetra, it is possible to define {\em sequential} vectors for
serial and parallel platforms. A sequential vector is a vector which, in
the opinion of the programmer, does not need to be partitioned among the
processes.  Note that each process defines its own sequential vectors,
and that changing an element of this vector on this process will {\em
  not} directly affect the vectors stored on other processes (if any
have been defined).

The class Epetra\_SerialDenseVector enables the construction and use of
real-valued, double precision dense vectors. The
Epetra\_SerialDenseVector class provides convenient vector notation but
derives all significant functionality from Epetra\_SerialDenseMatrix
class (see Section~\ref{sec:dense_mat}). The following instruction
creates a sequential double-precision vector containing {\tt Length}
elements:
\begin{verbatim}
#include "Epetra_SerialDenseVector.h"
Epetra_SerialDenseVector DoubleVector(Length);
\end{verbatim}
Other constructors are available, as described in the Epetra Class
Documentation.
Integer vectors can be created as
\begin{verbatim}
#include "Epetra_IntSerialDenseVector.h"
Epetra_SerialIntDenseVector IntVector(Length);
\end{verbatim}
We recomment Epetra\_SerialDenseVector and Epetra\_SerialIntDenseVector
instead of more common C++ allocations (using \verb!new!), because
Epetra serial vectors automatically delete the allocated memory when
destructed, avoiding possible memory leaks. 

The vector can be filled using the \verb![]! or \verb!()!  operators.
Both methods return a reference to the specified element of the vector.
However, using \verb!()!, bound checking is enforced. Using using
\verb![]!, no bounds checking is done unless Epetra is compiled with
\newline \verb!EPETRA_ARRAY_BOUNDS_CHECK!.

\begin{remark}
  To construct replicated Epetra objects on distributed memory machines,
  the user may consider the class Epetra\_LocalMap. The class constructs
  the replicated local objects and keeps information that describe the
  distribution.
\end{remark}

The file \TriExe{epetra/ex4.cpp} illustrates basic operations on dense
vectors.

%%%
%%%
%%%

\section{Creating and Assembling a Distributed Vector}
\label{sec:distr_vec}

A distributed object is an entity whose elements are partitioned across
more than one process. Epetra's distributed objects (derived from the
Epetra\_DistObject class) are created from a Map. For example, a
distributed vector can be constructed starting from an Epetra\_Map (or
Epetra\_BlockMap) with an instruction of type
\begin{verbatim}
Epetra_Vector x(Map);
\end{verbatim}
(We shall see that this dependency on Map objects holds for all
distributed Epetra objects.) This constructor allocates space for the
vector and sets all the elements to zero. A copy constructor may be used
as well:
\begin{verbatim}
Epetra_Vector y(x);
\end{verbatim}
A variety of sophisticated constructors are indeed available. For
instance, the user can pass a pointer to an array of double precision
values,
\begin{verbatim}
Epetra_Vector x(Copy,Map,LocalValues);
\end{verbatim}
Note the word \verb!Copy! is input to the constructor. It specifies the
Epetra\_CopyMode, and refers to many Epetra objects. In fact, Epetra
allows two data access modes:
\begin{enumerate}
\item \verb!Copy!: allocate memory and copy the user-provided data. In
  this mode, the user data is not needed be the new Epetra\_Vector after
  construction;
\item \verb!View!: create a ``view'' of the user's data. The user data
  is assumed to remain untouched for the life of the vector (or modified
  carefully). From a data hiding perspective, View mode is very
  dangerous. But is is often the only way to get the required
  performance. Therefore, users are strongly encouraged to develop code
  using the Copy mode. Only use View mode as needed in a secondary
  performance optimization phase. To use the View mode, the user has to
  define the vector entries using a (double) vector (of appropriate
  size), than construct an Epetra\_Vector with an instruction of type
\begin{verbatim}
  Epetra_Vector z(View,Map,z_values);
\end{verbatim}
  where \verb!z_values! is a pointer a double array containing the
  values for \verb!z!.
\end{enumerate}

To set a locally owned element of a vector, ont can use the \verb![]!
operator, regardless of how a vector has been created. For example,
\begin{verbatim}
x[i] = 1.0*i;
\end{verbatim}
where \verb!i! is in the local index space. 

Epetra also defines some functions to set vector elements in local or
global index space.  \verb!ReplaceMyValues! or \verb!SumIntoMyValues!
will replace or sum values into a vector with a given indexed list of
values, with indexes in the {\em local} index space;
\verb!ReplaceGlobalValues! or \newline
\verb!SumIntoGlobalValues! will replace or
sum values into a vector with a given indexed list of values in the {\em
  global} index space (but locally owned). It is important to note that
no process may set vector entries locally owned by another process. In
other words, both global and local insert and replace functions refer to
the part of a vector assigned to the calling process. Intra-process
communications can be (easily) performed using Import and Export
objects, covered in Section~\ref{sec:import_export}.

The user might need (for example, for reasons of computational
efficiency) to work on Epetra\_Vectors as if they were \verb!double *!
pointers.  File \newline \TriExe{epetra/ex5.cpp} \newline shows the use
of \verb!ExtractCopy()!.  \verb!ExtractCopy! does not give access to the
vector elements, but only copies them into the user-provided array.  The
user must commit those changes to the vector object, using, for
instance, \verb!ReplaceMyValues!.

A further computationally efficient way, is to extract a ``view'' of the
(multi-)vector internal data.  This can be done as follows, using method
\verb!ExtractView()!. Let \verb!z! be an Epetra\_Vector. 
\begin{verbatim}
double * z_values;
z.ExtractView( &z_values );
for( int i=0 ; i<MyLength ; ++i ) z_values[i] *= 10;
\end{verbatim}
In this way, modifying the values of \verb!z_values! will affect the
internal data of the Epetra\_Vector \verb!z!.  An example of the use of
\verb!ExtractView! is reported in file \newline \TriExe{epetra/ex6.cpp}.

\begin{remark}
  The class Epetra\_Vector is derived from Epetra\_MultiVector. Roughly
  speaking, a multi-vector is a collection of one or more vectors, all
  having the same length and distribution.  File \TriExe{epetra/ex7.cpp}
  illustrates use of multi-vectors.
\end{remark}

The user can also consider the function \verb!ResetView!, which allows a
(very) light-weight replacement of multi-vector values, created using
the Epetra\_DataMode \verb!View!. Note that no checking is performed to
see if the values passed in contain valid data. This method can be
extremely useful in the situation where a vector is needed for use with
an Epetra operator or matrix, and the user is not passing in a
multi-vector. Use this method with caution as it could be extremely
dangerous.  A simple example is reported in \newline
\TriExe{epetra/ex8.cpp}

\medskip

It is possible to perform a certain number of operations on vector
objects. Some of them are reported in Table~\ref{tab:distr_vec}.
Example \TriExe{epetra/ex18.cpp} works with some of the functions reported in
the table.

\begin{table}
\begin{center}
\begin{tabular}{ | p{15cm} | }
\hline
\verb!int NumMyELements()! \\
returns the local vector length on the
calling processor \\
\\
\verb!int NumGlobalElements()! \\
returns the  global length\\
\\
\verb!int Norm1(double *Result) const! \\
returns the 1-norm (defined as $\sum_i^n |
  x_i|$ (see also \verb!Norm2! and \verb!NormInf!)\\
\\
\verb!Normweigthed(double *Result) const! \\
 returns the  2-norm, defined as
$\sqrt{ \frac{1}{n} \sum_{j=1}^{n} (w_j x_j)^2}$) \\
\\
\verb!int Dot(const Epetra MultiVector A, double *Result) const! \\
 computes
the dot product of each corresponding pair of vectors \\
\\
\verb!int Scale(double ScalarA, const Epetra MultiVector &A! \\
Replace multi-vector values with scaled values of A,
\verb!this=ScalarA*A! \\
\\
\verb!int MinValue(double *Result) const! \\
compute minimum value of
each vector in multi-vector (see also \verb!MaxValue! and \verb!MeanValue!)\\
\\
\verb!int PutScalar(double Scalar)! \\
Initialize all values in a
multi-vector with constant value \\
\\
\verb!int Random()! \\
 set multi-vector values to random numbers \\
\hline
\end{tabular}
\caption{Some methods of the class {\tt Epetra\_Vector}}
\label{tab:distr_vec}
\end{center}
\end{table}

%%%
%%%
%%%

\section{Epetra\_Import and Epetra\_Export classes}
\label{sec:import_export}

The Epetra\_Import and Epetra\_Export classes apply off-processor
communication. Epetra\_Import and Epetra\_Export are used to construct a
communication plan that can be called repeatedly by computational
classes such the Epetra multi-vectors of the Epetra matrices.

Currently, those classes have one constructor, taking two Epetra\_Map
(or Epetra\_BlockMap) objects. The first map specifies the global IDs
that are owned by the calling processor. The second map specifies the
global IDs of elements that we want to import later.

Using an Epetra\_Import object means that the calling process knows what
it wants to receive, while an Epetra\_Export object means that it knows
what it wants to send. An Epetra\_Import object can be used to do an
Export as a reverse operation (and equivalently an Epetra\_Export can be
used to do an Import). In the particular case of bijective maps, either
Epetra\_Import or Epetra\_Export is appropriate.

\medskip

To better illustrate the use of these two classes, we present the
following example. Suppose that the double-precision distributed vector
\verb!x! of global length 4, is distributed over two processes. Process
0 own elements 0,1,2, while process 1 owns elements 1,2,3. This means
that elements 1 and 2 are replicated over the two processes. Suppose
that we want to bring all the components of \verb!x!  to process 0,
summing up the contributions of elements 1 and 2 from the 2 processes.
This is done in the following example (c.f.~\TriExe{epetra/ex9.cpp}).
\begin{verbatim}
  int NumGlobalElements = 4; // global dimension of the problem

  int NumMyElements; // local elements
  Epetra_IntSerialDenseVector MyGlobalElements;

  if( Comm.MyPID() == 0 ) {
    NumMyElements = 3;
    MyGlobalElements.Size(NumMyElements);
    MyGlobalElements[0] = 0;
    MyGlobalElements[1] = 1;
    MyGlobalElements[2] = 2;
  } else {
    NumMyElements = 3;
    MyGlobalElements.Size(NumMyElements);
    MyGlobalElements[0] = 1;
    MyGlobalElements[1] = 2;
    MyGlobalElements[2] = 3;
  }

  // create a double-precision map
  Epetra_Map Map(-1,MyGlobalElements.Length(),
                 MyGlobalElements.Values(),0, Comm);

  // create a vector based on map
  Epetra_Vector x(Map);
  for( int i=0 ; i<NumMyElements ; ++i )
    x[i] = 10*( Comm.MyPID()+1 );
  cout << x;

  // create a target map, in which all the elements are on proc 0
  int NumMyElements_target;

  if( Comm.MyPID() == 0 )
    NumMyElements_target = NumGlobalElements;
  else
    NumMyElements_target = 0;

  Epetra_Map TargetMap(-1,NumMyElements_target,0,Comm);

  Epetra_Export Exporter(Map,TargetMap);

  // work on vectors
  Epetra_Vector y(TargetMap);

  y.Export(x,Exporter,Add);
  cout << y;
\end{verbatim}

Running this code with 2 processors, the output will be approximatively
the following:
\begin{verbatim}
[msala:epetra]> mpirun -np 2 ./ex31.exe
Epetra::Vector
     MyPID           GID               Value
         0             0                      10
         0             1                      10
         0             2                      10
Epetra::Vector
         1             1                      20
         1             2                      20
         1             3                      20
Epetra::Vector
Epetra::Vector
     MyPID           GID               Value
         0             0                      10
         0             1                      30
         0             2                      30
         0             3                      20
\end{verbatim}

%%%
%%%
%%%

