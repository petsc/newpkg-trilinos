% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{MATLAB I/O with EpetraExt}
\label{chap:epetraext}

\ChapterAuthors{Marzio Sala}

\begin{introchapter}
EpetraExt is a collection of utilities for the handling of Epetra objects. 
In this
Chapter we will explain the usage of the input/output of Epetra
objects  to/from Matrix Market format. We will consider the following classes:
\begin{itemize}
\item EpetraExt::VectorToMatrixMarketFile (in Section \ref{sec:epetraext:vector});
\item EpetraExt::RowMatrixToMatrixMarketFile (in Section
                                              \ref{sec:epetraext:rowmatrix});
\end{itemize}
By using these classes, some Epetra objects can be easily exported to MATLAB.
\end{introchapter}

% ------------------------------------------------------------------------ %
\section{EpetraExt::VectorToMatrixMarketFile}
\label{sec:epetraext:vector}
% ------------------------------------------------------------------------ %

The Matrix Market format is a simple, portable and human-readable format. The
specifications to create the corresponding ASCII files can be found at
\begin{verbatim}
http://math.nist.gov/MatrixMarket/
\end{verbatim}
where MATLAB files to perform I/O can be downloaded. Let us consider that
\verb!X! is an Epetra\_Vector object. This object can be saved on file 
in both serial and parallel computations by using the command
\begin{verbatim}
EpetraExt::VectorToMatrixMarketFile(FileName, X, descr1, descr2);
\end{verbatim}
where \verb!FileName! is the output file name (for example, {\tt X.mm}), and \verb!descr1! and
\verb!descr2! are two description strings. By using the \verb!mmread.m! MATLAB
script available at the Matrix Market web site, one can type within MATLAB
\begin{verbatim}
>> X = mmread('X.mm');
\end{verbatim}
then use \verb!X! for any MATLAB operation. For multivectors, one should use
the class \\
      \verb!EpetraExt::MultiVectorToMatrixMarketFile! instead.

% ------------------------------------------------------------------------ %
\section{EpetraExt::RowMatrixToMatrixMarketFile}
\label{sec:epetraext:rowmatrix}
% ------------------------------------------------------------------------ %

Equivalently to what explained in Section~\ref{sec:epetraext:vector}, one can
export an Epetra\_RowMatrix derived class to a sparse MATLAB array. Let us
consider for example the following code, that creates a distributed diagonal
matrix:
\begin{verbatim}
  int NumMyElements = 5;
  Epetra_Map Map(-1, NumMyElements, 0, Comm);
  Epetra_CrsMatrix A(Copy, Map, 0);  // create a simple diagonal matrix  
  for (int i = 0 ; i < NumMyElements ; ++i)  
  {
    int j = Map.GID(i);
    double value = 1.0;    
    EPETRA_CHK_ERR(A.InsertGlobalValues(j, 1, &value, &j));
  }
  A.FillComplete();
\end{verbatim}
The matrix can be exported as follows:
\begin{verbatim}
EpetraExt::RowMatrixToMatrixMarketFile("A.mm", A, "test matrix", 
                                       "This is a test matrix");
\end{verbatim}
and then read in MATLAB as
\begin{verbatim}
>> A = mmread('A.mm')                                       

A =

   (1,1)        1
   (2,2)        1
   (3,3)        1
   (4,4)        1
   (5,5)        1
\end{verbatim}
This example is contained in \TriExe{epetraext/ex3.cpp}.

% --------------------------------------------------------------------------- %
\section{Concluding Remarks}
\label{sec:epetraext_concluding}
% --------------------------------------------------------------------------- %

EpetraExt contains other several classes not described here. Among them, we
note that one can read Epetra\_Map's, Epetra\_MultiVector's and
Epetra\_CrsMatrix's from file when previously saved using EpetraExt output
functions. Also, EpetraExt offers reordering and coloring. The Zoltan
interface is later described in Chapter~\ref{chap:zoltan}.
