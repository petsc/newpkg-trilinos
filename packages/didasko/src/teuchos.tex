%@HEADER
% ************************************************************************
% 
%          Trilinos: An Object-Oriented Solver Framework
%              Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%   
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details.
%   
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
% 
% Questions? Contact Michael A. Heroux (maherou@sandia.gov)
% 
% ************************************************************************
%@HEADER

\chapter{The Teuchos Utility Classes}
\label{chap:teuchos}

\ChapterAuthors{Heidi Thornquist}

\begin{introchapter}
Teuchos (pronounced ``te-fos'') is a collection of portable C++ tools
that facilitate the development of scientific codes.  Only a few of the many
tools in Teuchos are mentioned in this section.
For more details on all of the capabilities provided by Teuchos, please 
refer to the online documentation (\verb!http://trilinos.sandia.gov/packages/teuchos!).

Teuchos classes have been divided between a ``standard'' build and an
``extended'' build. The ``standard'' build contains the general purpose
templated tools like BLAS/LAPACK wrappers, parameter lists, a command-line parser,
serial dense matrices, timers, flop counters, and a reference-counted pointer class.  
These tools are built by default when Teuchos is enabled using the 
configure option \verb!--enable-teuchos!. 
The ``extended'' build contains more special purpose tools like 
XML parsing and MPI communicators, which can be included in the Teuchos
library by using the configure option \verb!--enable-teuchos-extended!.
\end{introchapter}

\section{Introduction}

In this Chapter, we will present the following ``standard'' build classes:
\begin{itemize}

\item \verb!Teuchos::ScalarTraits! class (Section~\ref{sec:teuchos:ScalarTraits}):
  The ScalarTraits class provides a basic interface to scalar types (float, double, 
  complex$<$float$>$, complex$<$double$>$) that is used by the templated computational
  classes within Teuchos.  It is the mechanism by which Teuchos' capabilities 
  can be extended to support arbitrary precisions.

\item \verb!Teuchos::SerialDenseMatrix! class (Section~\ref{sec:teuchos:SDM}): 
  The SerialDenseMatrix is a templated version of the \verb!Epetra_SerialDenseMatrix! class
  that is most often used to interface with the templated BLAS/LAPACK wrappers.

\item \verb!Teuchos::BLAS! class (Section~\ref{sec:teuchos:BLAS}):
  The BLAS class provides templated wrappers
  for the native BLAS library and can be extended to support arbitrary precision
  computations.  

\item \verb!Teuchos::LAPACK! class (Section~\ref{sec:teuchos:LAPACK}):
  The LAPACK class provides templated wrappers for the native LAPACK library.

\item \verb!Teuchos::ParameterList! class (Section~\ref{sec:teuchos:ParameterList}):
  ParameterList is a container that can be used to group all the parameters required by a
  given piece of code.

\item \verb!Teuchos::RCP! class (Section~\ref{sec:teuchos:RCP}):
  RCP is a smart reference-counted pointer class, which provides a functionality
  similar to the garbage collector of Java. 

\item \verb!Teuchos::TimeMonitor! class (Section~\ref{sec:teuchos:TimeMonitor}):
  TimeMonitor is a timing class that starts a timer when it is initialized and
  stops it when the destructor is called on the class.

\item \verb!Teuchos::CommandLineProcessor! class (Section~\ref{sec:teuchos:CLP}): 
  CommandLineProcessor is a class that helps parse command line input arguments from 
  \verb!(argc,argv[])!.   
\end{itemize}

%%%
%%%
%%%

\section{Teuchos::ScalarTraits}
\label{sec:teuchos:ScalarTraits}

The ScalarTraits class provides a basic interface to scalar types (float, double, 
complex$<$float$>$, complex$<$double$>$) that is used by the templated 
computational classes within Teuchos.  This interface includes a definition of
the magnitude type and methods for obtaining random numbers, representations of 
zero and one, the square root, and machine-specific parameters.  
The Teuchos classes that utilize this scalar traits mechanism are 
\verb!Teuchos::SerialDenseMatrix!, \verb!Teuchos::BLAS!, and \verb!Teuchos::LAPACK!.  

ScalarTraits enables the extension of Teuchos' computational
capabilities to any scalar type that can support its basic interface.  In particular, 
this interface can be used for arbitrary precision scalar types.  An interface to the
arbitrary precision library ARPREC \cite{arprec:02} is available if Teuchos
is configured with \verb!--enable-teuchos-arprec!. Teuchos must also be configured
with the local ARPREC library paths (\verb!--with-libs!, \verb!--with-incdirs!, and 
\verb!--with-libdirs!).  To obtain more information on ARPREC or download the 
source code, see {\tt http://crd.lbl.gov/$\sim$dhbailey/mpdist/}.

\begin{remark} To enable complex arithmetic (complex$<$float$>$ or complex$<$double$>$) 
support in ScalarTraits or any dependent classes, configure Teuchos with {\tt --enable-teuchos-complex}.
\end{remark}

%%%
%%%
%%%

\section{Teuchos::SerialDenseMatrix}
\label{sec:teuchos:SDM}

\verb!Teuchos::SerialDenseMatrix! is a templated version of the SerialDenseMatrix class in \verb!Epetra!
(Chapter \ref{chap:epetra_mat}).  It is most useful for interfacing with the templated BLAS and 
LAPACK wrappers, which will be discussed in Sections \ref{sec:teuchos:BLAS} and \ref{sec:teuchos:LAPACK}.  
However, by enabling the simple construction and manipulation of small dense matrices, 
the SerialDenseMatrix class has also been used as an independent tool in many 
Trilinos packages.

\verb!Teuchos::SerialDenseMatrix! provides a serial interface to a small dense matrix
of templated scalar type.  This means a SerialDenseMatrix object can be created for any scalar type 
supported by Teuchos::ScalarTraits (Section \ref{sec:teuchos:ScalarTraits}).  Boundschecking
can be enabled for this class by configuring Teuchos with {\tt --enable-teuchos-abc}.
An exception will be thrown every time a matrix bound is violated by any method.  This 
incurs a lot of overhead for this class, so boundschecking is only recommended as a debugging tool.

To use the Teuchos::SerialDenseMatrix class, include the header:

{\small 
\begin{verbatim}
#include "Teuchos_SerialDenseMatrix.hpp"
\end{verbatim}}
Creating a double-precision matrix can be done in several ways:
{\small 
\begin{verbatim}
// Create an empty matrix with no dimension
Teuchos::SerialDenseMatrix<int,double> Empty_Matrix;
// Create an empty 3x4 matrix
Teuchos::SerialDenseMatrix<int,double> My_Matrix( 3, 4 );
// Basic copy of My_Matrix
Teuchos::SerialDenseMatrix<int,double> My_Copy1( My_Matrix ),
// (Deep) Copy of principle 3x3 sub-matrix of My_Matrix
                  My_Copy2( Teuchos::Copy, My_Matrix, 3, 3 ),
// (Shallow) Copy of 2x3 sub-matrix of My_Matrix
                  My_Copy3( Teuchos::View, My_Matrix, 2, 3, 1, 1 );
\end{verbatim}}
The matrix dimensions and strided storage information can be obtained:
{\small
\begin{verbatim}
int rows = My_Copy3.numRows();  // number of rows
int cols = My_Copy3.numCols();  // number of columns
int stride = My_Copy3.stride(); // storage stride
\end{verbatim}}
Matrices can change dimension:
{\small
\begin{verbatim}
Empty_Matrix.shape( 3, 3 );     // size non-dimensional matrices
My_Matrix.reshape( 3, 3 );      // resize matrices and save values
\end{verbatim}}
Filling matrices with numbers can be done in several ways:
{\small 
\begin{verbatim}
My_Matrix.random();             // random numbers
My_Copy1.putScalar( 1.0 );      // every entry is 1.0
My_Copy2(1,1) = 10.0;           // individual element access
Empty_Matrix = My_Matrix;       // copy My_Matrix to Empty_Matrix 
\end{verbatim}}
Basic matrix arithmetic can be performed:
{\small
\begin{verbatim}
Teuchos::SerialDenseMatrix<int,double> My_Prod( 3, 2 );
// Matrix multiplication ( My_Prod = 1.0*My_Matrix*My_Copy^T )
My_Prod.multiply( Teuchos::NO_TRANS, Teuchos::TRANS, 
                  1.0, My_Matrix, My_Copy3, 0.0 );
My_Copy2 += My_Matrix;         // Matrix addition
My_Copy2.scale( 0.5 );         // Matrix scaling
\end{verbatim}}
The pointer to the array of matrix values can be obtained:
{\small
\begin{verbatim}
double* My_Array = My_Matrix.values();   // pointer to matrix values
double* My_Column = My_Matrix[2];        // pointer to third column values
\end{verbatim}}
The norm of a matrix can be computed:
{\small
\begin{verbatim}
double norm_one = My_Matrix.normOne();        // one norm
double norm_inf = My_Matrix.normInf();        // infinity norm
double norm_fro = My_Matrix.normFrobenius();  // frobenius norm
\end{verbatim}}
Matrices can be compared:
{\small
\begin{verbatim}
// Check if the matrices are equal in dimension and values
if (Empty_Matrix == My_Matrix) {
  cout<< "The matrices are the same!" <<endl;
}
// Check if the matrices are different in dimension or values
if (My_Copy2 != My_Matrix) {
  cout<< "The matrices are different!" <<endl;
}
\end{verbatim}}
A matrix can be sent to the output stream:
{\small
\begin{verbatim}
cout<< My_Matrix << endl;
\end{verbatim}}
This section presents examples of all the methods in the 
{\tt Teuchos::SerialDenseMatrix} class and can be found in
\TriExe{teuchos/ex1.cpp}.  There is also a specialization of
this class for serial dense vectors that includes additional creation, accessor, 
arithmetic, and norm methods ({\tt Teuchos::SerialDenseVector}).

%%%
%%%
%%%

\section{Teuchos::BLAS}
\label{sec:teuchos:BLAS}

The \verb!Teuchos::BLAS! class provides templated wrappers for the native BLAS library.
This class has been written to facilitate the interface between C++ codes and BLAS,
which are written in Fortran.  Unfortunately, the interface between C++ and Fortran
function calls is not standard across all computer platforms.  The \verb!Teuchos::BLAS!
class provides C++ wrappers for BLAS kernels that are specialized during the Teuchos
configuration.  This insulates the rest of Teuchos and its users from the details of
the Fortran to C++ translation.

The \verb!Teuchos::BLAS! class provides C++ wrappers for a substantial subset of the 
BLAS kernels (Figure \ref{blas_kernels}).
The native BLAS library implementations of those kernels
will be used for the standard scalar types (float, double, complex$<$float$>$, complex$<$double$>$).  
However, \verb!Teuchos::BLAS! also has a
templated version of each of these kernels.  Paired with \verb!Teuchos::ScalarTraits! 
(Section \ref{sec:teuchos:ScalarTraits}), the \verb!Teuchos::BLAS! class can be extended 
to provide arbitrary precision computations.  
To use the \verb!Teuchos::BLAS! class, 
include the header:
{\small 
\begin{verbatim}
#include "Teuchos_BLAS.hpp"
\end{verbatim}}
Creating an instance of the BLAS class for double-precision kernels looks like:
{\small 
\begin{verbatim}
Teuchos::BLAS<int, double> blas;
\end{verbatim}}
This instance provides the access to all the BLAS kernels listed in Figure \ref{blas_kernels}:
{\small
\begin{verbatim}
const int n = 10;
double alpha = 2.0;
double x[ n ];
for ( int i=0; i<n; i++ ) { x[i] = i; }
blas.SCAL( n, alpha, x, 1 );
int max_idx = blas.IAMAX( n, x, 1 );
cout<< "The index of the maximum magnitude entry of x[] is the "
    <<  max_idx <<"-th and x[ " << max_idx-1 << " ] = "<< x[max_idx-1] 
    << endl;
\end{verbatim}}
This is a small usage example, but its purpose is to illustrate that any of the supported 
BLAS kernels is a method of the {\tt Teuchos::BLAS} class.  
This example can be found in \TriExe{teuchos/ex2.cpp}.  

\begin{figure}[hbt]\centerline{\small{
\begin{tabular}{|l||l|}\hline
\bf{BLAS Kernel} & \bf{Description} \\\hline\hline
\_ROTG & Computes a Givens plane rotation \\\hline
\_SCAL & Scale a vector by a constant \\\hline
\_COPY & Copy one vector to another \\\hline
\_AXPY & Add one scaled vector to another \\\hline
\_ASUM & Sum the absolute values of the vector entries \\\hline
\_DOT  & Compute the dot product of two vectors \\\hline
\_NRM2 & Compute the 2-norm of a vector \\\hline
\_IAMAX & Determine the index of the largest magnitude entry of a vector \\\hline
\_GEMV & Add a scaled matrix-vector product to another scaled vector \\\hline
\_TRMV & Replaces a vector with its upper/lower-triangular matrix-vector product \\\hline
\_GER  & Updates a matrix with a scaled, rank-one outer product \\\hline
\_GEMM & Add a scaled matrix-matrix product to another scaled matrix \\\hline
\_SYMM & Add a scaled symmetric matrix-matrix product to another scaled matrix \\\hline
\_TRMM & Add a scaled upper/lower-triangular matrix-matrix product to another scaled matrix \\\hline
\_TRSM & Solves an upper/lower-triangular linear system with multiple right-hand sides \\\hline
\end{tabular}}}
\caption{BLAS kernels supported by Teuchos::BLAS}\label{blas_kernels}
\end{figure}

%%%
%%%
%%%

\section{Teuchos::LAPACK}
\label{sec:teuchos:LAPACK}

The \verb!Teuchos::LAPACK! class provides templated wrappers for the native LAPACK library.
This class has been written to facilitate the interface between C++ codes and BLAS,
which are written in Fortran.  Unfortunately, the interface between C++ and Fortran
function calls is not standard across all computer platforms.  The \verb!Teuchos::LAPACK!
class provides C++ wrappers for LAPACK routines that are specialized during the Teuchos
configuration.  This insulates the rest of Teuchos and its users from the details of
the Fortran to C++ translation.

\verb!Teuchos::LAPACK! is a serial interface only, as LAPACK functions
are. Users interested in the parallel counterpart of LAPACK, ScaLAPACK,
can use the Amesos package; see Chapter~\ref{chap:amesos}.

The \verb!Teuchos::LAPACK! class provides C++ wrappers for a substantial subset of the 
LAPACK routines (Figure \ref{lapack_routines}).
The native LAPACK library implementations of those kernels
will be used for the standard scalar types (float, double, complex$<$float$>$, complex$<$double$>$).  
Unlike \verb!Teuchos::BLAS!, the \verb!Teuchos::LAPACK! class does not have a templated version of 
these routines at this time, so it cannot offer arbitrary precision computations.

To use the \verb!Teuchos::LAPACK! class, include the header:
{\small 
\begin{verbatim}
#include "Teuchos_LAPACK.hpp"
\end{verbatim}}
Creating an instance of the LAPACK class for double-precision routines looks like:
{\small 
\begin{verbatim}
Teuchos::LAPACK<int, double> lapack;
\end{verbatim}}
This instance provides the access to all the LAPACK routines listed in Figure \ref{lapack_routines}:
{\small
\begin{verbatim}
Teuchos::SerialDenseMatrix<int,double> My_Matrix(4,4);
Teuchos::SerialDenseVector<int,double> My_Vector(4);
My_Matrix.random();
My_Vector.random();

// Perform an LU factorization of this matrix. 
int ipiv[4], info;
char TRANS = 'N';
lapack.GETRF( 4, 4, My_Matrix.values(), My_Matrix.stride(), ipiv, &info ); 

// Solve the linear system.
lapack.GETRS( TRANS, 4, 1, My_Matrix.values(), My_Matrix.stride(), 
	      ipiv, My_Vector.values(), My_Vector.stride(), &info );
\end{verbatim}}

This small example illustrates how easy it is to use the {\tt Teuchos::LAPACK}
class.  Furthermore, it also exhibits the compatibility of the {\tt Teuchos::SerialDenseMatrix} 
and {\tt Teuchos::-}\newline {\tt SerialDenseVector} classes with the {\tt Teuchos::LAPACK} class.  
This example can be found in \newline \TriExe{teuchos/ex3.cpp}.  

\begin{figure}[hbt]\centerline{\footnotesize{
\begin{tabular}{|l||l|}\hline
\bf{LAPACK Routine} & \bf{Description} \\\hline\hline
\_POTRF & Computes Cholesky factorization of a real symmetric positive definite (SPD) matrix. \\\hline
\_POTRS & Solves a system of linear equations where the matrix has been factored by POTRF. \\\hline
\_POTRI & Computes the inverse of a real SPD matrix after its been factored by POTRF. \\\hline
\_POCON & Estimates the reciprocal of the condition number (1-norm) of a real SPD matrix \\
 & after its been factored by POTRF. \\\hline
\_POSV  & Computes the solution to a real system of linear equations where the matrix is SPD. \\\hline
\_POEQU & Computes row and column scaling or equilibrating a SPD matrix and reduce \\
 & its condition number. \\\hline
\_PORFS & Improves the computed solution to a system of linear equations where the matrix is SPD. \\\hline
\_POSVX & Expert SPD driver:  Uses POTRF/POTRS to compute the solution to a real system of \\
 & linear equations where the matrix is SPD.  The system can be equilibrated (POEQU) or \\
 & iteratively refined (PORFS) also.\\\hline
\_GELS & Solves and over/underdetermined real linear system. \\\hline
\_GETRF & Computes an LU factorization of a general matrix using partial pivoting. \\\hline
\_GETRS & Solves a system of linear equations using the LU factorization computed by GETRF. \\\hline
\_GETRI & Computes the inverse of a matrix using the LU factorization computed by GETRF. \\\hline
\_GECON & Estimates the reciprocal of the condition number of a general matrix in either \\
 & the 1-norm or $\infty$-norm using the LU factorization computed by GETRF. \\\hline
\_GESV  & Computes the solution of a linear system of equations. \\\hline
\_GEEQU & Computes row and column scaling for equilibrating a linear system, reducing its \\
 & condition number. \\\hline
\_GERFS & Improves the computed solution to a system of linear equations and provides error \\
 & bounds and backward error estimates for the solution [ Use after GETRF/GETRS ].\\\hline
\_GESVX & Expert driver:  Uses GETRF/GETRS to compute the solution to a real system of linear \\
 & equations, returning error bounds on the solution and a condition estimate. \\\hline
\_GEHRD & Reduces a real general matrix to upper Hessenberg form by orthogonal similarity \\
 & transformations \\\hline
\_HSEQR & Compute the eigenvalues of a real upper Hessenberg matrix and, optionally, the \\
 & Schur decomposition. \\\hline
\_GEES & Computes the real Schur form, eigenvalues, and Schur vectors of a real nonsymmetric \\
 & matrix. \\\hline
\_GEEV & Computes the eigenvalues and, optionally, the left and/or right eigenvectors \\
 & of a real nonsymmetric matrix. \\\hline
\_ORGHR & Generates a real orthogonal matrix which is the product of the elementary reflectors \\
 & computed by GEHRD. \\\hline
\_ORMHR & Overwrites the general real matrix with the product of itself and the elementary \\
 & reflectors computed by GEHRD. \\\hline
\_TREVC & Computes some or all of the right and/or left eigenvectors of a real upper \\
 & quasi-triangular matrix. \\\hline
\_TREXC & Reorders the real Schur factorization of a real matrix via orthogonal similarity \\
 & transformations. \\\hline
\_LARND & Returns a random number from a uniform or normal distribution. \\\hline
\_LARNV & Returns a vector of random numbers from a chosen distribution. \\\hline
\_LAMCH & Determines machine parameters for floating point characteristics. \\\hline
\_LAPY2 & Computes sqrt ($x^2$ + $y^2$) safely, to avoid overflow. \\\hline
\end{tabular}}}
\caption{LAPACK routines supported by Teuchos::LAPACK}\label{lapack_routines}
\end{figure}

\clearpage
%%%
%%%
%%%

\section{Teuchos::ParameterList}
\label{sec:teuchos:ParameterList}

The {\tt Teuchos::ParameterList} class is a C++ container of $<${\it key}, {\it value}$>$ pairs, 
where the {\it key} is a character string ({\tt std::string}) and the {\it value} can be almost 
any type of C++ object.  The ability to hold almost any type of C++ object as a {\it value} 
in the same list makes this class very useful for storing parameters.  This parameter list can then
be passed to an object, like an iterative linear solver, which will use the information to define 
its behavior. 

The \verb!Teuchos::ParameterList! is currently being used by several Trilinos
packages.  For instance, all Amesos objects~(see Chapter
\ref{chap:amesos}) and the smoothed aggregation preconditioning object
\verb!ML_Epetra::MultiLevelPreconditioner! (see Section~\ref{sec:ml:preconditioner}) 
are configured through a \verb!Teuchos::ParameterList!.

\begin{remark}
The parameter list stores a copy of the input object if it passed by reference.  
If the list is passed a pointer to an object, only the pointer is copied and
not the object that it points to. 
\end{remark}

To use the \verb!Teuchos::ParameterList! class, include the header:
{\small 
\begin{verbatim}
#include "Teuchos_ParameterList.hpp"
\end{verbatim}}
Creating an empty parameter list looks like:
{\small 
\begin{verbatim}
Teuchos::ParameterList My_List;
\end{verbatim}}
Setting parameters in this list can be easily done:
{\small
\begin{verbatim}
My_List.set("Max Iters", 1550);
My_List.set("Tolerance", 1e-10);
My_List.set("Solver", "GMRES");
\end{verbatim}}
The templated ``set'' method should cast the input {\it value} to the
correct data type.  However, in the case where the compiler is not casting the input
{\it value} to the expected data type, an explicit cast can be used with the ``set'' method:
{\small
\begin{verbatim}
My_List.set("Tolerance", (float)(1e-10));
\end{verbatim}}
A hierarchy of parameter lists can be constructed using {\tt Teuchos::ParameterList}.  This 
means another parameter list is a valid {\it value} in any parameter list.  To create a sublist
in a parameter list and obtain a reference to it:
{\small
\begin{verbatim}
Teuchos::ParameterList& Prec_List = My_List.sublist("Preconditioner");
\end{verbatim}}
Now this parameter list can be filled with values:
{\small
\begin{verbatim}
Prec_List.set("Type", "ILU");
Prec_List.set("Drop Tolerance", 1e-3);
\end{verbatim}}
The parameter list can be queried about the existence of a parameter, sublist, or type:
{\small
\begin{verbatim}
// Has a solver been chosen?
bool solver_defined = My_List.isParameter("Solver");
// Has a preconditioner been chosen?
bool prec_defined = My_List.isSublist("Preconditioner"); 
// Has a tolerance been chosen and is it a double-precision number?
bool tol_double = My_List.template isType<double>("Tolerance");
// Has a drop tolerance been chosen and is it a double-precision number?
bool dtol_double = Teuchos::isParameterType<double>(Prec_List,
                                                    "Drop Tolerance"); 
\end{verbatim}}
\noindent The last two methods for checking the parameter type are equivalent.
There is some question as to whether the syntax of the first type-checking
method ({\tt isType}) is acceptable to older compilers.  Thus, the second type-checking 
method ({\tt isParameterType}) is offered as a portable alternative.

Parameters can be retrieved from the parameter list in quite a few ways:
{\small
\begin{verbatim}
// Get method that creates and sets the parameter if it doesn't exist.
int its = My_List.get("Max Iters", 1200);
// Get method that retrieves a parameter of a particular type.
float tol = My_List.template get<float>("Tolerance");
\end{verbatim}}
\noindent In the above example, the first ``get'' method is a safe way of
obtaining a parameter when its existence is indefinite but required.
The second ``get'' method should be used when the existense of the parameter
is definite.  This method will throw an exception if the parameter doesn't exist. 
The safest way to use the second ``get'' method
is in a try/catch block:
{\small
\begin{verbatim}
try {
tol = My_List.template get<float>("Tolerance");
}
catch (exception& e) {
tol = 1e-6;
}
\end{verbatim}}
\noindent The second ``get'' method uses a syntax that may not be
acceptable to older compilers.  Optionally, there is another portable templated 
``get'' function that can be used in the place of the second ``get'' method:
{\small
\begin{verbatim}
try {
tol = Teuchos::getParameter<float>(My_List, "Tolerance");
}
catch (exception& e) {
tol = 1e-6;
}
\end{verbatim}}

A parameter list can be sent to the output stream:
{\small
\begin{verbatim}
cout<< My_List << endl;
\end{verbatim}}
\noindent For this parameter list, the output would look like:
{\small
\begin{verbatim}
Max Iters = 1550
Preconditioner ->
  Drop Tolerance = 0.001   [unused]
  Type = ILU   [unused]
Solver = GMRES   [unused]
Tolerance = 1e-10
\end{verbatim}}
\noindent It is important to note that misspelled parameters 
(with additional space characters, capitalizations, etc.) may be ignored.  
Therefore, it is important to be aware that a given parameter has not been used. 
Unused parameters can be printed with method:
{\small
\begin{verbatim}
My_List.unused( cout );
\end{verbatim}}
This section presents examples of all the methods in the 
{\tt Teuchos::ParameterList} class and can be found in
\TriExe{teuchos/ex4.cpp}.  

%%%
%%%
%%%

\section{Teuchos::RCP}
\label{sec:teuchos:RCP}

The \verb!Teuchos::RCP! class is a templated class for implementing
automatic garbage collection in C++ using smart, reference-counted pointers.
Using this class allows one client to dynamically create an object and pass
the object around to other clients without fear of memory leaks.  
No client is required to explicitly
call delete because object will be deleted when all the clients remove their
references to it.  The type of garbage collection performed by \verb!Teuchos::RCP! is similar to those found in Perl and Java.

To use the \verb!Teuchos::RCP! class, include the header:
{\small 
\begin{verbatim}
#include "Teuchos_RCP.hpp"
\end{verbatim}}
The data type used with {\tt Teuchos::RCP} should not 
be a built-in data type (like {\tt int} or {\tt double}), this creates unnecessary 
overhead.  Instead, it should be used to manage dynamic objects 
and data members that need to be shared with many clients.  This means that the data type 
will most likely be a C++ class.  Consider the class hierarchy:
{\small
\begin{verbatim}
class A { 
 public: 
   A() {}
   virtual ~A(){} 
   virtual void f(){} 
};   
class B1 : virtual public A {};
class B2 : virtual public A {};
class C : public B1, public B2 {};
\end{verbatim}}

Creating a reference-counted pointer to a dynamically allocated object (of type {\tt A}) can be done several ways:

{\small 
\begin{verbatim}
// Create a reference-counted NULL pointer of type A.
RCP<A>	           a_null_ptr;
// Create a reference-counted pointer of non-const type A.
RCP<A>             a_ptr = rcp(new A);
// Create a reference-counted pointer of const type A.
RCP<const A>       ca_ptr = rcp(new A);
// Create a const reference-counted pointer of non-const type A.
const RCP<A>       a_cptr = rcp(new A); 
// Create a const reference-counted pointer of const type A.
const RCP<const A> ca_cptr = rcp(new A); 
\end{verbatim}}

The {\tt Teuchos::RCP} class can also perform implicit conversions between a derived
class ({\tt B1}) and its base class ({\tt A}):

{\small
\begin{verbatim}
RCP<B1> b1_ptr  = rcp(new B1);
RCP<A> a_ptr1 = b1_ptr;
\end{verbatim}}

\noindent Other non-implicit type conversions like static, dynamic, or const casts
can be taken care of by non-member template functions:

{\small
\begin{verbatim}
RCP<const C> c_ptr = rcp(new C);
// Implicit cast from C to B2.
RCP<const B2> b2_ptr = c_ptr;                              
// Safe cast, type-checked, from C to A.
RCP<const A> ca_ptr1 = rcp_dynamic_cast<const A>(c_ptr); 
// Unsafe cast, non-type-checked, from C to A.
RCP<const A> ca_ptr2 = rcp_static_cast<const A>(c_ptr);  
// Cast away const from B2.
RCP<B2>       nc_b2_ptr = rcp_const_cast<B2>(b2_ptr);           
\end{verbatim}}

Using a reference-counted pointer is very similar to using a raw C++ pointer.  Some
of the operations that are common to both are:
{\small
\begin{verbatim}
RCP<A>
   a_ptr2 = rcp(new A), // Initialize reference-counted pointers.
   a_ptr3 = rcp(new A); // ""
A  *ra_ptr2 = new A,    // Initialize non-reference counted pointers.
   *ra_ptr3 = new A;    // ""
a_ptr2 = rcp(ra_ptr3);  // Assign from a raw pointer (only do this once!)
a_ptr3 = a_ptr2;        // Assign one smart pointer to another.
a_ptr2 = rcp(ra_ptr2);  // Assign from a raw pointer (only do this once!)
a_ptr2->f();            // Access a member of A using ->
ra_ptr2->f();           // ""
*a_ptr2 = *a_ptr3;      // Dereference the objects and assign.
*ra_ptr2 = *ra_ptr3;    // ""
\end{verbatim}}

\noindent However, a reference-counted pointer cannot be used everywhere a raw C++ pointer
can.  For instance, these statements will not compile:
{\small
\begin{verbatim}
// Pointer arithmetic ++, --, +, - etc. not defined!
a_ptr1++;            // error  
// Comparison operators ==, !=, <=, >= etc. not defined!
a_ptr1 == ra_ptr1;   // error 
\end{verbatim}}

\noindent Because the two are not equivalent, the {\tt Teuchos::RCP} class provides 
a way of getting the raw C++ pointer held by any {\tt RCP<A>} object:
{\small
\begin{verbatim}
A* true_ptr = a_ptr1.get();
\end{verbatim}}
These are just some of the basic features found in the {\tt Teuchos::RCP} class.  
A more extensive tutorial of this powerful tool is ``in the works'' and will be made available
to Teuchos users as soon as it is finished.  The examples presented in this section can be found in
\TriExe{teuchos/ex5.cpp}.  

%%%
%%%
%%%

\section{Teuchos::TimeMonitor}
\label{sec:teuchos:TimeMonitor}

The \verb!Teuchos::TimeMonitor! class is a container that manages a
group of timers.  In this way, it can be used to keep track of timings
for various phases of the code.  Internally, this class holds an array 
of \verb!Teuchos::Time! objects.  The 
\verb!Teuchos::Time! class defines a basic wall-clock timer that can 
\verb!start()!, \verb!stop()!, and return the \verb!totalElapsedTime()!.

To use the \verb!Teuchos::TimeMonitor! class, include the header:
{\small 
\begin{verbatim}
#include "Teuchos_TimeMonitor.hpp"
\end{verbatim}}
To create a timer for the TimeMonitor to manage, call:
{\small 
\begin{verbatim}
RCP<Time> FactTime = TimeMonitor::getNewTimer("Factorial Time");
\end{verbatim}}
\noindent The {\tt getNewTimer} method creates a new reference-counted
{\tt Teuchos::Time} object and adds it to the internal array.  To avoid
passing this timer into each method that needs timing, consider putting it in the
global scope (declare it outside of {\tt main(argc, argv[])}).  Now, when 
we want to time a part of the code, the appropriate timer should be used to 
construct a local TimeMonitor:
{\small
\begin{verbatim}
Teuchos::TimeMonitor LocalTimer(*FactTime);
\end{verbatim}}
\noindent This timer will be started during the construction of {\tt LocalTimer}
and stopped when the destructor is called on {\tt LocalTimer}.

To obtain a summary from all the timers in the global TimeMonitor, use:
{\small
\begin{verbatim}
TimeMonitor::summarize();
\end{verbatim}}
\noindent Information from each timer can also be obtained using 
the methods from {\tt Teuchos::Time}.  
This section presents examples of all the 
methods in the {\tt Teuchos::TimeMonitor} class and can be found in
\TriExe{teuchos/ex6.cpp}.  

%%%
%%%
%%%

\section{Teuchos::CommandLineProcessor}
\label{sec:teuchos:CLP}

\verb!Teuchos::CommandLineProcessor! is a class that helps to parse command
line input arguments and set runtime options. Additionally, a CommandLineProcessor
object can provide the user with a list of acceptable command line arguments, and
their default values.

To use the \verb!Teuchos::CommandLineProcessor! class, include the header:
{\small 
\begin{verbatim}
#include "Teuchos_CommandLineProcessor.hpp"
\end{verbatim}}
Creating an empty command line processor looks like:
{\small 
\begin{verbatim}
Teuchos::CommandLineProcessor My_CLP;
\end{verbatim}}
To set and option, it must be given a name and default value.  Additionally,
each option can be given a help string.  Although it is not necessary, a help
string aids a users comprehension of the acceptable command line arguments.
Some examples of setting command line options are:
{\small
\begin{verbatim}    
// Set an integer command line option.
int NumIters = 1550;
My_CLP.setOption("iterations", &NumIters, "Number of iterations");
// Set a double-precision command line option.
double Tolerance = 1e-10;    
My_CLP.setOption("tolerance", &Tolerance, "Tolerance");
// Set a string command line option.
string Solver = "GMRES";
My_CLP.setOption("solver", &Solver, "Linear solver");
// Set a boolean command line option.    
bool Precondition;
My_CLP.setOption("precondition","no-precondition",
               &Precondition,"Preconditioning flag");
\end{verbatim}}
There are also two methods that control the strictness of the command line processor.
For a command line processor to be sensitive to any bad command line option that it does
not recognize, use:
{\small
\begin{verbatim}
My_CLP.recogniseAllOptions(false);
\end{verbatim}}
\noindent Then, if the parser finds a command line option it doesn't recognize, it will
throw an exception.  To prevent a command line processor from throwing an exception when
it encounters a unrecognized option or when the help string is printed, use:
{\small
\begin{verbatim}
My_CLP.throwExceptions(false);
\end{verbatim}}
Finally, to parse the command line, {\tt argc} and {\tt argv} are passed to the {\tt parse} method:
{\small
\begin{verbatim}    
My_CLP.parse( argc, argv );
\end{verbatim}}
The {\tt --help} output for this command line processor is:
{\small
\begin{verbatim}
Usage: ./ex7.exe [options]
  options:
  --help                         Prints this help message
  --pause-for-debugging          Pauses for user input to allow 
                                 attaching a debugger
  --iterations           int     Number of iterations
                                 (default: --iterations=1550)
  --tolerance            double  Tolerance
                                 (default: --tolerance=1e-10)
  --solver               string  Linear solver
                                 (default: --solver="GMRES")
  --precondition         bool    Preconditioning flag
  --no-precondition              (default: --precondition)
\end{verbatim}}
\noindent This section presents examples of all the methods in the 
{\tt Teuchos::CommandLineProcessor} class and can be found in
\TriExe{teuchos/ex7.cpp}.  

%%%
%%%
%%%

%\subsection{Teuchos::XMLObject}
%\label{sec:teuchos:XML}

%Teuchos contains few classes to parse a subset of the XML syntax. Users
%can easily create XML objects, as reported in file
%\TriExe{teuchos/ex4.cpp}. 
%As internal data, XML objects can be view as an alternative the
%Teuchos::ParameterList. Another use of XML parser is to read input file.

%Let us start from the definition of an internal XML object to setup a
%linear system solver:
%\begin{verbatim}
%XMLObject solver("solver");
%XMLObject prec("preconditioner");

%solver.addAttribute("krylov method", "gmres");
%solver.addInt("iterations", 1000);
%solver.addDouble("tolerance", 1.0e-10);

%solver.addChild(prec);
%prec.addAttribute("type", "smoothed aggregation");
%prec.addInt("max levels", 4);
%\end{verbatim}
%The content of the XML object can be printed out as
%\begin{verbatim}
%string str = solver.toString();
%cout << str << endl;
%\end{verbatim}
%An XML object can be queries using a variety of methods, like
%\verb!hasAttribute()!, \verb!getAttribute()!, \verb!getRequiredInt()!,
%\verb!getRequiredDouble()!, \verb!getChild()!.

%XML can be read from file in the following way. This example, reported
%in file \TriExe{teuchos/ex5.cpp}, requires option \verb!--with-expat!.

%\begin{remark}
%  \verb!Teuchos::StrUtils! is another class offered by Teuchos to read
%  ASCII files.
%\end{remark}
