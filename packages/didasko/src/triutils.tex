% @HEADER
% ***********************************************************************
% 
%            Trilinos: An Object-Oriented Solver Framework
%                 Copyright (2001) Sandia Corporation
% 
% Under terms of Contract DE-AC04-94AL85000, there is a non-exclusive
% license for use of this work by or on behalf of the U.S. Government.
% 
% This library is free software; you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as
% published by the Free Software Foundation; either version 2.1 of the
% License, or (at your option) any later version.
%  
% This library is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
%  
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
% USA
% Questions? Contact Michael A. Heroux (maherou@sandia.gov) 
% 
% ***********************************************************************
% @HEADER

\chapter{Generating Linear Systems with Triutils}
\label{chap:triutils}

\ChapterAuthors{Marzio Sala}

\begin{introchapter}
This Chapter presents two functionalities of Triutils, that will be
extensively used in the examples of the later chapters: 
\begin{itemize}
\item the Triutils command line parser (in
  Section~\ref{sec:triutils:CLP});
\item the Triutils matrix generator (in Section~\ref{sec:triutils:gallery}).
\end{itemize}
%Readers not interested in the examples may decide to skip this chapter.
Some readers may choose to skip this Chapter because their application
is their example. However, it does help to find the test matrices
closest to the ones in their code for several reasons.  Using
well-chosen matrices, simple but sufficiently close to the final
application, one can quickly test the performances of a given set of
algorithms on the problem of interest, using a serial or a parallel
environment. Several test matrices exhibit a known behavior (e.g.,
theory predicts the behavior of the condition number), and can be used
to validate algorithms. Besides, as they can be quickly generate with
few code lines, experts may use them to optimize or fix thier code.
Therefore, a short code using a gallery matrix may be used to
communicate with developers.
\end{introchapter}

%%%
%%%
%%%

\section{Trilinos\_Util::CommandLineParser}
\label{sec:triutils:CLP}

It is possible to use the \newline
\verb!Trilinos_Util::CommandLineParser! class to parse the command line.
With this class, it is easy to handle input line arguments and
shell-defined variables. For instance, the user can write
\begin{verbatim}
[msala:triutils]>ex2.exe -nx 10 -tol 1e-6 -solver=cg -output
\end{verbatim}
and, in the code, easily obtain the value of {\tt nx}, {\tt tol}, and
{\tt solver}, using a simple code as follows:
\begin{verbatim}
int main(int argc, char *argv[])
{

  Trilinos_Util::CommandLineParser CLP(argc,argv);
  int nx = CLP.Get("-nx", 123);
  int ny = CLP.Get("-ny", 145);
  double tol = CLP.Get("-tol", 1e-12);
  string solver = CLP.Get("-solver","gmres");

  bool Output = CLP.Has("-output");

  cout << "nx = " << nx << endl;
  cout << "ny = " << ny << " (default value)" << endl;
  cout << "tol = " << tol << endl;
  cout << "solver = " << solver << endl;

  return 0;
}
\end{verbatim}
In the command line, the user can specify a value for a given option in
the following ways:
\begin{itemize}
\item \verb!-tolerance 1e-12! (with one or more spaces)
\item \verb!-tolerance=1e-12! (with \verb!=! sign and no spaces)
\end{itemize}

Option names must begin with one or more dashes (`\verb!-!'). Options
may have at most one value.
 
If option name is not found in the database, the default value is
returned. If needed, the user can also specify a default value to return
when the option name is not found in the database. The method
\verb!HaveOption! will query the database for an option.

File \TriExe{triutils/ex2.cpp} gives an example of the usage of this class.
 
%%%
%%%
%%%

\section{Trilinos\_Util::CrsMatrixGallery}
\label{sec:triutils:gallery}

The class \verb!Trilinos_Util::CrsMatrixGallery! provides functions
similar to the MATLAB's \verb!gallery!  function\footnote{Many of the
  matrices that can be created using Trilinos\_Util::CrsMatrixGallery
  are equivalent or simiilar to those provided by the
  MATLAB\copyright~function {\tt gallery}. In these cases, the reader is
  referred to the MATLAB documentation for more details about the
  matrices' properties.}.

A typical constructor requires the problem type and an Epetra\_Comm, and
is followed by a set of instructions to specify the problem.  The
following example creates a matrix corresponding to the discretization
of a 2D Laplacian on a Cartesian grid with 100 points:
\begin{verbatim}
Trilinos_Util::CrsMatrixGallery Gallery("laplace_2d", Comm);
Gallery.Set("problem_size",100);
Gallery.Set("map_type","linear");
Gallery.Set("exact_solution","random");
\end{verbatim}
The nodes are decomposed linearly, and the exact solution is a random
vector.

The next example will read a matrix stored in Harwell/Boeing format:
\begin{verbatim}
Trilinos_Util::CrsMatrixGallery Gallery("hb", Comm);
Gallery.Set("matrix_name","bcsstk14.rsa");
Gallery.Set("map_type","greedy");
\end{verbatim}
The example reads the matrix (and, if available, solution and right-hand
side) from the file \verb!bcsstk14.rsa!, and partitions the matrix
across the processes using a simple greedy algorithm.

\bigskip

Once all the required parameters have been specified, the user can get a
pointer to the constructed Epetra\_CrsMatrix, to the exact and starting
solution, and to the right-hand side (both Epetra\_Vector's):
\begin{verbatim}
A = Gallery.GetMatrix();
ExactSolution = Gallery.GetExactSolution();
RHS = Gallery.GetRHS();
StartingSolution = Gallery.GetStartingSolution();
\end{verbatim}
An Epetra\_LinearProblem is defined by
\begin{verbatim}  
Epetra_LinearProblem Problem(A,StartingSolution,RHS);
\end{verbatim}
Next one may use AztecOO to solve the linear system:
\begin{verbatim}  
AztecOO Solver(Problem);
Solver.SetAztecOption( AZ_precond, AZ_dom_decomp );  
Solver.Iterate(1000,1E-9);
\end{verbatim}
Using Trilinos\_Util::MatrixGallery, one computes the true
residual and the difference between computed and exact solution:
\begin{verbatim} 
double residual;
Gallery.ComputeResidual(&residual);
  
Gallery.ComputeDiffBetweenStartingAndExactSolutions(&residual);
\end{verbatim}
A list of methods implemented by Trilinos\_Util::CrsMatrixGallery is
reported in Table~\ref{tab:triutils:crs}.

\begin{table}[htbp]
  \centering
  \begin{tabular}{| p{14cm} |}
    \hline
    {\tt GetMatrix()}\\
    {Returns a pointer to the internally
      stored Epetra\_CrsMatrix.} \\ 
    \\
    \tt GetExactSolution() \\
    {Returns a pointer to the internally
      stored exact solution vector (as an Epetra\_Vector).} \\
    \\
    {\tt GetStartingSolution()} \\
    {Returns a pointer to the
      internally stored starting solution vector (as an
      Epetra\_Vector).} \\
    \\
    {\tt GetRhs()}  \\
    {Returns a pointer to the internally
      stored right-hand side (as an Epetra\_Vector).} \\
    \\
    {\tt GetLinearProblem()} \\
    {Returns a pointer to the internally
      stored  Epetra\_LinearProblem for the VBR matrix).} \\
    \\
    {\tt GetMap()}\\
    {Returns a pointer to the internally
      stored Epetra\_Map.} \\
    \hline
  \end{tabular}
  \caption{Methods of class Trilinos\_Util::CrsMatrixGallery.}
  \label{tab:triutils:crs}
\end{table}
The matrix can be written on a file in MATLAB format, using
\begin{verbatim}
string FileName = "matrix.m";
bool UseSparse = false;
Gallert.WriteMatrix(FileName,UseSparse);
\end{verbatim}
If \verb!UseSparse! is true, the matrix is created in sparse format
(using the MATLAB command \verb!spalloc!).

\medskip

To sum up, the main options reviewed here are:

\vskip .1 in

\choicebox{\tt problem\_type}{[{\tt string}] Specifies the problem
  type. A list of currently available problems is reported later in this
  section.}

\choicebox{\tt problem\_size}{[{\tt int}] Size of the problem. Note
  that some problems, defined on structured meshes, allow the
  specification of the number of nodes on the x-, y- and z-axis. Please
  refer to each problem's description for more details.}

\choicebox{\tt nx}{[{\tt int}] Number of nodes in the x-direction (if
  supported by the specific problem).}

\choicebox{\tt ny}{[{\tt int}] Number of nodes in the y-direction (if
  supported by the specific problem).}

\choicebox{\tt nz}{[{\tt int}] Number of nodes in the z-direction (if
  supported by the specific problem).}

\choicebox{\tt mx}{[{\tt int}] Number of processes in the x-direction (if
  supported by the specific problem).}

\choicebox{\tt my}{[{\tt int}] Number of processes in the y-direction
  (if supported by the specific problem).}

\choicebox{\tt mz}{[{\tt int}] Number of processes in the z-direction
  (if supported by the specific problem).}

\choicebox{\tt map\_type}{[{\tt string}] Defines the data layout across
  the processes. See Table~\ref{tab:triutils:map}. }
 
\choicebox{\tt exact\_solution}{[{\tt string}] Defines the exact solution. See
  Table~\ref{tab:triutils:exact}.}

\choicebox{\tt starting\_solution}{[{\tt string}] Defines the starting solution
  vector. It can be: {\tt zero} or {\tt random}.}


\begin{table}[htbp]
  \centering
  \begin{tabular}{| p{4cm} | p{10cm} |}
    \hline
 {\tt linear} & {Create a linear map. Elements are divided into
  continuous chunks among the processors. This is the default value.} \\
{\tt box} & {Used for problems defined on Cartesian grids over a
  square. The domain is subdivided into {\tt mx x my} subdomains.  {\tt mx}
  and {\tt my} are automatically computed if the total number of
  processes is a perfect square. Alternatively, {\tt
    mx} and {\tt my} are specified via {\tt Set("mx",IntValue)} and {\tt
    Set("my",IntValue)}.} \\
{\tt interlaced} & {Elements are subdivided so that element i is
  assigned to process i\%NumProcs.} \\
{\tt random} & {Assign each node to a random process} \\
{\tt greedy} & {(only for HB matrices) implements a greedy
  algorithm to decompose the graph of the HB matrix among the processes}
\\
\hline
  \end{tabular}
  \caption{Available {\tt map\_type} optins.}
  \label{tab:triutils:map}
\end{table}

\begin{table}[htbp]
  \centering
  \begin{tabular}{| p{4cm} | p{10cm} |}
    \hline
    {\tt random} & Random values \\
    {\tt constant}  & All elements set to 1.0. \\
    {\tt quad\_x} & Nodes are supposed to be distributed over the 1D segment $(0,1)$, with
  equal spacing, and the solution is computed as $x(1-x)$. \\
{\tt quad\_xy} & Nodes are supposed to be distributed over the square
  $(0,1) \times (0,1)$ , with equal spacing, and the solution is
  computed as $x(1-x)y(1-y)$. \\
    \hline
  \end{tabular}
  \caption{Available {\tt exact\_solution} options.}
  \label{tab:triutils:exact}
\end{table}

A list of currently available problems is reported below. We use the
following notation.  \verb!IntValue! always refer to a generic positive
integer. The following symbols {\tt a,b,c,d,e,f,g} always refer to
double-precision values.  Note that some matrices are dense, but still
stored as Epetra\_CrsMatrix, a sparse matrix format. The generic $(i,j)$
element of a given matrix is $A_{i,j}$ (for simplicity, we suppose that
indices start from 1\footnote{It is understood that, in the actual
  implementation, indices start from 0.}). $n$ represents the matrix
size.

\vskip .1 in

\choicebox{\tt eye}{Creates an identity matrix. The size of the
  problem is set using {\tt Set("problem size", IntValue)}, or,
  alternatively, by {\tt Set("nx", IntValue)}.}


\choicebox{\tt cauchy}{Creates a particular instance of a Cauchy matrix
  with elements $A_{i,j} = 1/(i+j)$. Explicit formulas are known for the
  inverse and determinant of a Cauchy matrix. For this particular Cauchy
  matrix, the determinant is nonzero and the matrix is totally
  positive.}

\choicebox{\tt cross\_stencil\_2d}{Creates a matrix with the same stencil of
  {\tt laplace\_2d}, but with arbitrary values. The stencil is
  \[
  A =   \left[
    \begin{tabular}{ccc}
    & e &  \\
  b & a & c \\
    & d &   \\
  \end{tabular}
  \right] .
  \]
  The default values are {\tt a=5, b=c=d=e=1}.
  The problem size is specified as in {\tt laplace\_2d}.}

\choicebox{\tt cross\_stencil\_3d}{Similar to the 2D case. The matrix
  stencil correspond to that of a 3D Laplace operator on a structured
  grid. On a given x-y plane, the stencil is as in {\tt laplace 2d}. The
  value on the plane below is set using {\tt Set("f",F)}, and in the
  plane above with {\tt Set("g",G")}.  The default values are {\tt
    a=7,b=c=d=e=f=g=1}. The problem size is specified as in {\tt
    laplace3d}.}

\choicebox{\tt diag}{Creates a diagonal matrix. The elements on the
  diagonal can be set using {\tt Set("a",value)}. Default value is {\tt
    a = 1}. The problem size is set as for {\tt eye}.}

\choicebox{\tt fiedler}{Creates a matrix whose element are $A_{i,j} = |
  i - j |$.  The matrix is symmetric, and has a dominant positive
  eigenvalue, and all the other eigenvalues are negative.}

\choicebox{\tt hanowa}{Creates a matrix whose eigenvalues lie on a
  vertical line in the complex plane. The matrix has the 2x2 block
  structure (in MATLAB's notation)
  \[
  A = \left[
    \begin{tabular}{cc} a * eye(n/2) & -diag(1:m) \\
        diag(1:m) &     a * eye(n/2) \\
\end{tabular}
      \right].
      \]
      The complex eigenvalues are of the form a $k \sqrt{-1}$ and $-k
      \sqrt{-1}$, for $1 \leq k \leq n/2$.  The default value for {\tt
        a} is -1.}

\choicebox{\tt hb}{The matrix is read from file. File name is specified
  by {\tt Set("file name", FileName)}. {\tt FileName} is a C++ string.
  The problem size is automatically determined.}



\choicebox{\tt hilbert}{This is a famous example of a badly conditioned
  matrix. The elements are defined as $A_{i,j} = 1/(i+j)$.}

\choicebox{\tt jordblock}{Creates a Jordan block with eigenvalue set via
  {\tt Set("a",DoubleVal)}. The default value is 0.1. The problem size
  is specified as for {\tt eye}.}
    
\choicebox{\tt kms}{Create the $n \times n$ Kac-Murdock-Szeg\"o
  Toepliz matrix such that $A_{i,j} = \rho^{|i-j|}$ (for real $\rho$
  only).  Default value is $\rho= 0.5$, or can be set as {\tt
    Set("a",value)}. The inverse of this matrix is tridiagonal, and
  the matrix is positive definite if and only if $0 < |\rho| < 1$.}

\choicebox{\tt laplace\_1d}{Creates the classical tridiagonal matrix
  with stencil [-1, 2, -1].  The problem size is specified as for {\tt
    eye}.}

\choicebox{\tt laplace\_1d\_n}{As for {\tt laplace\_1d}, but with Neumann
  boundary condition. The matrix is singular. }

\choicebox{\tt laplace\_2d}{Creates a matrix corresponding to the
  stencil of a 2D Laplacian operator on a structured Cartesian grid.
  The problem size is specified using {\tt Set("problem size",
    IntValue)}.  In this case, IntValue must be a perfect square.
  Alternatively, one can set the number of nodes along the x-axis and
  y-axis, using {\tt Set("nx",IntValue)} and {\tt Set("ny",IntValue)}.}

\choicebox{\tt laplace\_2d\_n}{As for {\tt laplace\_2d}, but with Neumann
  boundary condition. The matrix is singular.}

\choicebox{\tt laplace\_3d}{Creates a matrix corresponding to the
  stencil of a 3D Laplacian operator on a structured Cartesian grid. The
  problem size is specified using {\tt Set("problem size",IntValue)}. In
  this case, IntValue must be a cube. Alternatively, one can specify the
  number of nodes along the axis, using {\tt Set("nx",IntValue),
    Set("ny",IntValue)}, and {\tt Set("nz",IntValue)}.}

\choicebox{\tt lehmer}{Returns a symmetric positive definite matrix, such
  that
  \[
  A_{i,j} = 
  \left\{
    \begin{array}{ll}
      \frac{i}{j} & \mbox{ if } j \ge i \\
      \frac{j}{i} & \mbox{ otherwise } \\
    \end{array}
  \right. .
  \]
  This matrix has three properties: is totally nonnegative, the inverse
  is tridiagonal and explicitly known, The condition number is bounded
  as $ n \le cond(A) \le 4*n$. The problem size is set as for {\tt
    eye}.}



\choicebox{\tt minij}{Returns the symmetric positive definite matrix
  defined as $A_{i,j} = \min(i,j)$. The problem size is set as for {\tt
    eye}.}

\choicebox{\tt ones}{Creates a matrix with equal elements. The default
  value is 1, and cab be changed using {\tt Set("a",a)}.}

\choicebox{\tt parter}{Creates a matrix $A_{i,j} = 1/(i-j+0.5)$.  This
  matrix is a Cauchy and a Toepliz matrix. Most of the singular values
  of A are very close to $\pi$. The problem size is set as for {\tt
    eye}.}

\choicebox{\tt pei}{Creates the matrix 
\[
A_{i,j} = \left\{ \begin{array}{cc}
\alpha + 1 & \mbox{ if } i \neq j \\
1  & \mbox{ if } i = j.
\end{array}
\right. .
\]
The value of $\alpha$ can be set as {\tt Set("a",value)}, and it
defaults to 1. This matrix is singular for $\alpha = 0$ or $-n$.}


\choicebox{\tt recirc\_2d}{Returns a matrix corresponding to the
  finite-difference discretization of the problem
  \[
- \mu \Delta u + (v_x,v_y) \cdot \nabla u = f
\]
on the unit square, with homogeneous Dirichlet boundary conditions. A
standard 5-pt stencil is used to discretize the diffusive term, and a
simple upwind stencil is used for the convective term. Here,
\[
v_x = ( y - 1/2 ) V, \quad \quad \quad v_y = ( 1/2 - x ) V
\]
The value of $\mu$ can be specified using {\tt Set("diff",
  DoubleValue)}, and that of $V$ using {\tt Set("conv", DoubleValues)}.
The default values are $\mu=10^{-5}, V=1$.  The problem size is
specified as in {\tt laplace\_3d}.}


\choicebox{\tt ris}{Returns a symmetric Hankel matrix with elements
  $A_{i,j} = 0.5/(n-i-j+1.5)$, where $n$ is problem size. The
  eigenvalues of A cluster around $-\pi/2$ and $\pi/2$.}

\choicebox{\tt tridiag}{Creates a tridiagonal matrix. The diagonal
  element is set using {\tt Set("a", a)}, the subdiagonal using {\tt
    Set("b",b)}, and the superdiagonal using {\tt Set("c",c)}. The
  default values are {\tt a=2, b=c=1}. The problem size is specified as
  for {\tt eye}.}

\choicebox{\tt uni\_flow\_2d}{Returns a matrix corresponding to the
  finite-difference discretization of the problem
  \[
- \mu \Delta u + (v_x,v_y) \cdot \nabla u = f
\]
on the unit square, with homogeneous Dirichlet boundary conditions. A
standard 5-pt stencil is used to discretize the diffusive term, and a
simple upwind stencil is used for the convective term. Here,
\[
v_x = cos(\alpha) V, \quad \quad \quad v_y =  sin(\alpha) V 
\]
that corresponds to an unidirectional 2D flow.  The value of $\mu$ can
be specified using {\tt Set("diff", DoubleValue)}, and that of $V$ using
{\tt Set("conv", DoubleValue)}, and that of $\alpha$ using {\tt
  Set("alpha", DoubleValue)}. The default values are $V=1, \mu=10^{-5},
\alpha=0$. The problem size is specified as in {\tt laplace3d}.}

%\choicebox{\tt vander}{Create the Vandermonde matrix whose second last
%  column is the vector c. The j-th column is given by $A(:,j) =
%  \alpha^{(n-j)}$. The value of $\alpha$ can be set as {\tt
%    Set("a",value)}, and it is defaulted to 0.5. }

\medskip

Class Trilinos\_Util::VrbMatrixGallery, derived from
Trilinos\_Util::CrsMatrixGallery, can be used to generate VBR matrices.
The class creates an Epetra\_CrsMatrix (following user's defined
parameters, as previously specified), then ``expands'' this matrix into
a VBR matrix. This VBR  matrix is based on an Epetra\_BlockMap, based on
the Epetra\_Map used to define the Epetra\_CrsMatrix. The number of PDE
equations per node is set with parameter \verb!num_pde_eqns!. The
Epetra\_CrsMatrix is expanded into a VBR matrix by replicating the
matrix \verb!num_pde_eqns! times for each equation.

A list of methods implemented by Trilinos\_Util::VrbMatrixGallery is
reported in Table~\ref{tab:triutils:vbr}.

\begin{table}[htbp]
  \centering
  \begin{tabular}{| p{14cm} |}
    \hline
    {\tt GetVrbMatrix()}\\
    {Returns a pointer to the internally
  stored VBR matrix.} \\
\tt GetVrbExactSolution() \\
{Returns a pointer to the internally
  stored exact solution vector (as an Epetra\_Vector).} \\
{\tt GetVrbStartingSolution()} \\
 {Returns a pointer to the
  internally stored starting solution vector (as an Epetra\_Vector).} \\
{\tt GetVrbRhs()}  \\
 {Returns a pointer to the internally
  stored right-hand side (as an Epetra\_Vector).} \\
{\tt GetVrbLinearProblem()} \\
 {Returns a pointer to the internally
  stored  Epetra\_LinearProblem.} \\
{\tt GetBlockMap()}\\
 {Returns a pointer to the internally
  stored Epetra\_BlockMap.} \\
    \hline
  \end{tabular}
  \caption{Methods of class Trilinos\_Util::VbrMatrixGallery.}
  \label{tab:triutils:vbr}
\end{table}

\medskip

Trilinos\_Util::CrsMatrixGallery can be used in conjuction with
Trilinos\_Util::CommandLineParser as in the following code:
\begin{verbatim}
int main(int argc, char *argv[]) 
{
 #ifdef HAVE_MPI
  MPI_Init(&argc,&argv);
  Epetra_MpiComm Comm(MPI_COMM_WORLD);
#else
  Epetra_SerialComm Comm;
#endif

  Epetra_Time Time(Comm);

  Trilinos_Util::CommandLineParser CLP(argc,argv);
  Trilinos_Util::CrsMatrixGallery Gallery("", Comm);

  Gallery.Set(CLP);

  // get matrix
  Epetra_CrsMatrix * Matrix = Gallery.GetMatrix();
  Epetra_Vector * LHS = Gallery.GetLHS();
  Epetra_Vector * StartingSolution = Gallery.GetStartingSolution();
  Epetra_Vector * ExactSolution = Gallery.GetExactSolution();
  Epetra_LinearProblem * Problem =  Gallery.GetLinearProblem();

  // various computatons...

  // check that computed solution (in StartingSolution) 
  // is close enough to ExactSolution

  double residual, diff;

  Gallery.ComputeResidual(&residual);
  Gallery.ComputeDiffBetweenStartingAndExactSolutions(&diff);
  
  if( Comm.MyPID()==0 ) 
    cout << "||b-Ax||_2 = " << residual << endl;

  if( Comm.MyPID()==0 ) 
    cout << "||x_exact - x||_2 = " << diff << endl;

#ifdef HAVE_MPI
  MPI_Finalize() ;
#endif

  return 0 ;

}
\end{verbatim}
This program can be executed with the following command line:
\begin{verbatim}
[msala:triutils]> mpirun -np 4 ex1.exe -problem_type=laplace_2d \
                  -problem_size=10000
\end{verbatim}
Matrix gallery option names shall be specified with an additional
leading dash (``\verb!-!''). Options values will be specified as usual.

\begin{remark}
  Most of the examples reported in the following chapters use both\newline
  Trilinos\_Util::CommandLineParser and Trilinos\_Util::CrsMatrixGallery
  to define the distributed matrix. The user is encouraged to test a
  given method using matrices with different numerical properties.
\end{remark}

