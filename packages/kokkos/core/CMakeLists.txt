

TRIBITS_SUBPACKAGE(Core)

ADD_SUBDIRECTORY(src)

TRIBITS_ADD_TEST_DIRECTORIES(unit_test)
TRIBITS_ADD_TEST_DIRECTORIES(perf_test)

TRIBITS_ADD_EXAMPLE_DIRECTORIES(usecases)

TRIBITS_SUBPACKAGE_POSTPROCESS()

