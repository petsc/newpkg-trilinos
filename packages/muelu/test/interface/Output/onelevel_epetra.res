Level 0
 Setup Smoother (MueLu::IfpackSmoother{type = point relaxation stand-alone})
 [empty list]
 

--------------------------------------------------------------------------------
---                            Multigrid Summary                             ---
--------------------------------------------------------------------------------
Number of levels    = 1
Operator complexity = 1.00

matrix rows    nnz  nnz/row procs
A 0    9999  29995     3.00  1

Smoother (level 0) pre  : MueLu::IfpackSmoother{type = point relaxation stand-alone}
Smoother (level 0) post : no smoother

