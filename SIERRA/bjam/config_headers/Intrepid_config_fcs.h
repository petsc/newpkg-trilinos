/* src/Intrepid_config.h.in.  Generated from configure.ac by autoheader.  */

/* Define if want to build with intrepid enabled */
#define HAVE_INTREPID_AMESOS

/* Define if want to build with amesos enabled */
#define HAVE_INTREPID_AMESOS_UMFPACK

/* Define if want to build intrepid-debug */
/* #undef HAVE_INTREPID_DEBUG */

/* Define if want to build intrepid-debug with inf and nan checking */
/* #undef HAVE_INTREPID_DEBUG_INF_CHECK */

/* Define if want to build with intrepid enabled */
#define HAVE_INTREPID_EPETRA

/* Define if want to build with intrepid enabled */
#define HAVE_INTREPID_EPETRAEXT

/* Define if want to build with intrepid enabled */
/* #undef HAVE_INTREPID_SACADO */
